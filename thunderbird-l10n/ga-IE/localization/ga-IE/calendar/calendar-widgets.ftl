# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-view-toggle-day = Lá
    .title = Oscail amharc lae
calendar-view-toggle-week = Seachtain
    .title = Oscail amharc seachtaine
calendar-view-toggle-multiweek = Amharc Ilseachtaine
    .title = Oscail amharc ilseachtaine
calendar-view-toggle-month = Mí
    .title = Oscail amharc míosa
