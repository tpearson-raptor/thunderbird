# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Флажок для переключения выбора текущей строки
    .title = Выбрать текущую строку
tree-list-view-row-deselect =
    .alt = Флажок для переключения выбора текущей строки
    .title = Снять выбор текущей строки
tree-list-view-row-delete =
    .title = Удалить текущую строку
tree-list-view-column-picker =
    .title = Выбрать колонки для отображения
tree-list-view-column-picker-restore =
    .label = Восстановить порядок столбцов
tree-list-view-row-thread =
    .alt = Индикатор сообщения в обсуждении
    .title = Это сообщение из обсуждения
tree-list-view-row-flagged =
    .alt = Индикатор помеченных сообщений
    .title = Сообщение помечено
tree-list-view-row-flag =
    .alt = Индикатор помеченных сообщений
    .title = Сообщение не помечено
tree-list-view-row-attach =
    .alt = Индикатор вложений
    .title = Сообщение содержит вложения
tree-list-view-row-spam =
    .alt = Индикатор спам-состояния
    .title = Сообщение помечено как спам
tree-list-view-row-not-spam =
    .alt = Индикатор спам-состояния
    .title = Сообщение не помечено как спам
