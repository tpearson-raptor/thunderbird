# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Добро пожаловать в <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Центр учётных записей

## Footer

account-hub-release-notes = Примечания к выпуску
account-hub-support = Поддержка
account-hub-donate = Пожертвовать

## Start page

account-hub-email-button = Настроить учётную запись электронной почты
account-hub-new-email-button = Получить новый адрес электронной почты
account-hub-calendar-button = Настроить календарь
account-hub-address-book-button = Настроить адресную книгу
account-hub-chat-button = Настроить чат
account-hub-feed-button = Настроить RSS-канал
account-hub-newsgroup-button = Настроить группу новостей
account-hub-import-button = Импортировать профиль
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Войти для Синхронизации…

## Email page

account-hub-email-title = Настройте свою учётную запись электронной почты
account-hub-email-cancel-button = Отмена
account-hub-email-back-button = Назад
account-hub-email-continue-button = Продолжить
account-hub-email-confirm-button = Подтвердить
