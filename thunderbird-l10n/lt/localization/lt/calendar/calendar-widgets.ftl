# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Šiuo metu visi kalendoriai yra išjungti. Įjunkite turimą kalendorių arba pridėkite naują, kad galėtumėte kurti ir redaguoti įvykius.
calendar-deactivated-notification-tasks = Šiuo metu visi kalendoriai yra išjungti. Įjunkite turimą kalendorių arba pridėkite naują, kad galėtumėte kurti ir redaguoti užduotis.
calendar-view-toggle-day = Diena
    .title = Rodyti dienos kalendorių
calendar-view-toggle-week = Savaitė
    .title = Rodyti savaitės kalendorių
calendar-view-toggle-multiweek = Kelios savaitės
    .title = Rodyti kelių savaičių kalendorių
calendar-view-toggle-month = Mėnuo
    .title = Rodyti mėnesio kalendorių
