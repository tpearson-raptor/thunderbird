# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Όλα τα ημερολόγια έχουν απενεργοποιηθεί αυτήν τη στιγμή. Ενεργοποιήστε ένα υπάρχον ημερολόγιο ή προσθέστε ένα νέο για να δημιουργήσετε και να επεξεργαστείτε εκδηλώσεις.
calendar-deactivated-notification-tasks = Όλα τα ημερολόγια έχουν απενεργοποιηθεί αυτήν τη στιγμή. Ενεργοποιήστε ένα υπάρχον ημερολόγιο ή προσθέστε ένα νέο για να δημιουργήσετε και να επεξεργαστείτε εργασίες.
calendar-notifications-label = Εμφάνιση ειδοποιήσεων για επερχόμενες εκδηλώσεις
calendar-add-notification-button =
    .label = Προσθήκη ειδοποίησης
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = Επιλογή ημερολογίου «{ $calendarName }»
calendar-import-new-calendar = Νέο ημερολόγιο…
    .title = Δημιουργήστε ή εγγραφείτε σε ένα νέο ημερολόγιο
calendar-refresh-calendars =
    .title = Επαναφόρτωση όλων των ημερολογίων και συγχρονισμός αλλαγών
calendar-new-event-primary-button = Νέα εκδήλωση
calendar-new-task-primary-button = Νέα εργασία
calendar-nav-button-prev-tooltip-day =
    .title = Προηγούμενη ημέρα
    .accesskey = γ
calendar-nav-button-prev-tooltip-week =
    .title = Προηγούμενη εβδομάδα
    .accesskey = γ
calendar-nav-button-prev-tooltip-multiweek =
    .title = Προηγούμενη εβδομάδα
    .accesskey = γ
calendar-nav-button-prev-tooltip-month =
    .title = Προηγούμενος μήνας
    .accesskey = γ
calendar-nav-button-next-tooltip-day =
    .title = Επόμενη ημέρα
    .accesskey = ν
calendar-nav-button-next-tooltip-week =
    .title = Επόμενη εβδομάδα
    .accesskey = ν
calendar-nav-button-next-tooltip-multiweek =
    .title = Επόμενη εβδομάδα
    .accesskey = ν
calendar-nav-button-next-tooltip-month =
    .title = Επόμενος μήνας
    .accesskey = ν
calendar-today-button-tooltip =
    .title = Μετάβαση στο σήμερα
calendar-view-toggle-day = Ημέρα
    .title = Εναλλαγή σε ημερήσια προβολή
calendar-view-toggle-week = Εβδομάδα
    .title = Εναλλαγή σε εβδομαδιαία προβολή
calendar-view-toggle-multiweek = Πολλές εβδομάδες
    .title = Εναλλαγή σε πολυεβδομαδιαία προβολή
calendar-view-toggle-month = Μήνας
    .title = Εναλλαγή σε μηνιαία προβολή
