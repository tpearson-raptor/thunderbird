# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Πλαίσιο επιλογής για εναλλαγή της επιλογής της τρέχουσας γραμμής
    .title = Επιλογή της τρέχουσας γραμμής
tree-list-view-row-deselect =
    .alt = Πλαίσιο επιλογής για την εναλλαγή επιλογής της τρέχουσας γραμμής
    .title = Αποεπιλέξτε την τρέχουσα γραμμή
tree-list-view-row-delete =
    .title = Διαγραφή της τρέχουσας γραμμής
tree-list-view-column-picker =
    .title = Επιλέξτε στήλες προς εμφάνιση
tree-list-view-column-picker-restore =
    .label = Επαναφορά σειράς στηλών
tree-list-view-row-thread =
    .alt = Δείκτης νήματος μηνυμάτων
    .title = Αυτό είναι μήνυμα νήματος
tree-list-view-row-flagged =
    .alt = Δείκτης μηνύματος με αστέρι
    .title = Το μήνυμα είναι με αστέρι
tree-list-view-row-flag =
    .alt = Δείκτης μηνύματος με αστέρι
    .title = Το μήνυμα δεν είναι με αστέρι
tree-list-view-row-attach =
    .alt = Δείκτης συνημμένου
    .title = Το μήνυμα περιέχει συνημμένα
tree-list-view-row-spam =
    .alt = Δείκτης ανεπιθύμητου μηνύματος
    .title = Το μήνυμα έχει οριστεί ως ανεπιθύμητο
tree-list-view-row-not-spam =
    .alt = Δείκτης ανεπιθύμητου μηνύματος
    .title = Το μήνυμα δεν έχει οριστεί ως ανεπιθύμητο
