# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Καλώς ορίσατε στο <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Κέντρο λογαριασμών

## Footer

account-hub-release-notes = Σημειώσεις έκδοσης
account-hub-support = Υποστήριξη
account-hub-donate = Δωρεά

## Start page

account-hub-email-button = Ρύθμιση λογαριασμού email
account-hub-new-email-button = Απόκτηση νέας διεύθυνσης email
account-hub-calendar-button = Ρύθμιση ημερολογίου
account-hub-address-book-button = Ρύθμιση ευρετηρίου
account-hub-chat-button = Ρύθμιση συνομιλίας
account-hub-feed-button = Ρύθμιση ροής RSS
account-hub-newsgroup-button = Ρύθμιση ομάδας συζητήσεων
account-hub-import-button = Εισαγωγή προφίλ
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Σύνδεση στο Sync…

## Email page

account-hub-email-title = Ρύθμιση λογαριασμού email
account-hub-email-cancel-button = Ακύρωση
account-hub-email-back-button = Πίσω
account-hub-email-continue-button = Συνέχεια
account-hub-email-confirm-button = Επιβεβαίωση
