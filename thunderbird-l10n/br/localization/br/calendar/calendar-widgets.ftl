# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-view-toggle-day = Devezh
    .title = Tremen d'ar gwel devezh
calendar-view-toggle-week = Sizhun
    .title = Tremen d'ar gwel sizhun
calendar-view-toggle-multiweek = Lies-sizhun
    .title = Tremen d'ar gwel lies-sizhun
calendar-view-toggle-month = Miz
    .title = Tremen d'ar gwel miz
