# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Velkomin í <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Reikningamiðstöð

## Footer

account-hub-release-notes = Útgáfuupplýsingar
account-hub-support = Aðstoð
account-hub-donate = Styrkja

## Start page

account-hub-email-button = Settu upp tölvupóstreikning
account-hub-new-email-button = Fáðu nýtt tölvupóstfang
account-hub-calendar-button = Settu upp dagatal
account-hub-address-book-button = Settu upp nafnaskrá
account-hub-chat-button = Settu upp spjall
account-hub-feed-button = Settu upp RSS-streymi
account-hub-newsgroup-button = Settu upp fréttahóp
account-hub-import-button = Flytja inn notandasnið
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Skráðu þig inn til að samstilla…

## Email page

account-hub-email-title = Settu upp tölvupóstreikninginn þinn
account-hub-email-cancel-button = Hætta við
account-hub-email-back-button = Til baka
account-hub-email-continue-button = Halda áfram
account-hub-email-confirm-button = Staðfesta
