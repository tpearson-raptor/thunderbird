# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Gátreitur til að skipta um val á núverandi línu
    .title = Velja núverandi línu
tree-list-view-row-deselect =
    .alt = Gátreitur til að skipta um val á núverandi línu
    .title = Velja núverandi línu
tree-list-view-row-delete =
    .title = Eyða núverandi línu
tree-list-view-column-picker =
    .title = Veldu dálka til að birta
tree-list-view-column-picker-restore =
    .label = Endurheimta röðun dálka
tree-list-view-row-thread =
    .alt = Vísir fyrir þráð skilaboða
    .title = Þetta eru skilaboð í spjallþræði
tree-list-view-row-flagged =
    .alt = Vísir fyrir stjörnumerkt skilaboð
    .title = Stjörnumerkt skilaboð
tree-list-view-row-flag =
    .alt = Vísir fyrir stjörnumerkt skilaboð
    .title = Ekki stjörnumerkt skilaboð
tree-list-view-row-attach =
    .alt = Vísir fyrir viðhengi
    .title = Skilaboð innihalda viðhengi
tree-list-view-row-spam =
    .alt = Stöðuvísir fyrir ruslpóst
    .title = Skilaboð merkt sem ruslpóstur
tree-list-view-row-not-spam =
    .alt = Stöðuvísir fyrir ruslpóst
    .title = Skilaboð ekki merkt sem ruslpóstur
