# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Öll dagatöl eru óvirk sem stendur. Virkjaðu fyrirliggjandi dagatal eða bættu við nýju til að búa til og breyta atburðum.
calendar-deactivated-notification-tasks = Öll dagatöl eru óvirk sem stendur. Virkjaðu fyrirliggjandi dagatal eða bættu við nýju til að búa til og breyta verkefnum.
calendar-notifications-label = Birta tilkynningar um komandi atburði
calendar-add-notification-button =
    .label = Bæta við tilkynningu

## Side panel

calendar-list-header = Dagatöl
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = Valkostur { $calendarName } dagatals
calendar-import-new-calendar = Nýtt dagatal…
    .title = Búa til eða gerast áskrifandi að nýju dagatali
calendar-refresh-calendars =
    .title = Endurnýja öll dagatöl og samstilla breytingar
calendar-new-event-primary-button = Nýr atburður
calendar-new-task-primary-button = Nýtt verkefni

## Calendar navigation

calendar-nav-button-prev-tooltip-day =
    .title = Fyrri dagur
    .accesskey = r
calendar-nav-button-prev-tooltip-week =
    .title = Fyrri vika
    .accesskey = r
calendar-nav-button-prev-tooltip-multiweek =
    .title = Fyrri vika
    .accesskey = r
calendar-nav-button-prev-tooltip-month =
    .title = Fyrri mánuður
    .accesskey = r
calendar-nav-button-next-tooltip-day =
    .title = Næsti dagur
    .accesskey = n
calendar-nav-button-next-tooltip-week =
    .title = Næsta vika
    .accesskey = n
calendar-nav-button-next-tooltip-multiweek =
    .title = Næsta vika
    .accesskey = n
calendar-nav-button-next-tooltip-month =
    .title = Næsti mánuður
    .accesskey = n
calendar-today-button-tooltip =
    .title = Fara á daginn í dag
calendar-view-toggle-day = Dagur
    .title = Skipta yfir í dagsýn
calendar-view-toggle-week = Vika
    .title = Skipta yfir í vikusýn
calendar-view-toggle-multiweek = Margar vikur
    .title = Skipta yfir í margra vikna sýn
calendar-view-toggle-month = Mánuður
    .title = Skipta yfir í mánaðarsýn
