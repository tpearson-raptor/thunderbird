# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = All calendars are currently disabled. Enable an existing calendar or add a new one to create and edit events.
calendar-deactivated-notification-tasks = All calendars are currently disabled. Enable an existing calendar or add a new one to create and edit tasks.
calendar-notifications-label = Show notifications for upcoming events
calendar-add-notification-button =
    .label = Add notification
calendar-view-toggle-day = Day
    .title = Switch to day view
calendar-view-toggle-week = Week
    .title = Switch to week view
calendar-view-toggle-multiweek = Multiweek
    .title = Switch to multiweek view
calendar-view-toggle-month = Month
    .title = Switch to month view
