# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-view-toggle-day = Hari
    .title = Tukar ke paparan hari
calendar-view-toggle-week = Minggu
    .title = Tukar ke paparan minggu
calendar-view-toggle-multiweek = Berbilang minggu
    .title = Tukar ke paparan berbilang minggu
calendar-view-toggle-month = Bulan
    .title = Tukar ke paparan bulan
