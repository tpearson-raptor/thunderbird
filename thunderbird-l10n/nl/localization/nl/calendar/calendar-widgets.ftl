# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Alle agenda’s zijn momenteel uitgeschakeld. Schakel een bestaande agenda in of voeg een nieuwe toe om afspraken te maken en te bewerken.
calendar-deactivated-notification-tasks = Alle agenda’s zijn momenteel uitgeschakeld. Schakel een bestaande agenda in of voeg een nieuwe toe om taken te maken en te bewerken.
calendar-notifications-label = Meldingen tonen voor aankomende afspraken
calendar-add-notification-button =
    .label = Melding toevoegen

## Side panel

calendar-list-header = Agenda’s
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = { $calendarName }-agendaoptie
calendar-import-new-calendar = Nieuwe agenda…
    .title = Een nieuwe agenda aanmaken of erop abonneren
calendar-refresh-calendars =
    .title = Alle agenda’s opnieuw laden en wijzigingen synchroniseren
calendar-new-event-primary-button = Nieuwe gebeurtenis
calendar-new-task-primary-button = Nieuwe taak

## Calendar navigation

calendar-nav-button-prev-tooltip-day =
    .title = Vorige dag
    .accesskey = r
calendar-nav-button-prev-tooltip-week =
    .title = Vorige week
    .accesskey = r
calendar-nav-button-prev-tooltip-multiweek =
    .title = Vorige week
    .accesskey = r
calendar-nav-button-prev-tooltip-month =
    .title = Vorige maand
    .accesskey = r
calendar-nav-button-next-tooltip-day =
    .title = Volgende dag
    .accesskey = l
calendar-nav-button-next-tooltip-week =
    .title = Volgende week
    .accesskey = l
calendar-nav-button-next-tooltip-multiweek =
    .title = Volgende week
    .accesskey = l
calendar-nav-button-next-tooltip-month =
    .title = Volgende maand
    .accesskey = l
calendar-today-button-tooltip =
    .title = Naar vandaag gaan
calendar-view-toggle-day = Dag
    .title = Wisselen naar dagoverzicht
calendar-view-toggle-week = Week
    .title = Wisselen naar weekoverzicht
calendar-view-toggle-multiweek = Meerdere weken
    .title = Wisselen naar overzicht van meerdere weken
calendar-view-toggle-month = Maand
    .title = Wisselen naar maandoverzicht
