# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Welkom bij <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Accounthub

## Footer

account-hub-release-notes = Uitgaveopmerkingen
account-hub-support = Ondersteuning
account-hub-donate = Doneren

## Start page

account-hub-email-button = E-mailaccount instellen
account-hub-new-email-button = Een nieuw e-mailadres aanvragen
account-hub-calendar-button = Agenda instellen
account-hub-address-book-button = Adresboek instellen
account-hub-chat-button = Chat instellen
account-hub-feed-button = RSS-feed instellen
account-hub-newsgroup-button = Nieuwsgroep instellen
account-hub-import-button = Profiel importeren
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Aanmelden bij Synchronisatie…

## Email page

account-hub-email-title = Uw e-mailaccount instellen
account-hub-email-cancel-button = Annuleren
account-hub-email-back-button = Terug
account-hub-email-continue-button = Doorgaan
account-hub-email-confirm-button = Bevestigen
