# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Aanvinkveld om de selectie van de huidige rij te wisselen
    .title = De huidige rij selecteren
tree-list-view-row-deselect =
    .alt = Aanvinkveld om de selectie van de huidige rij te wisselen
    .title = De huidige rij deselecteren
tree-list-view-row-delete =
    .title = De huidige rij verwijderen
tree-list-view-column-picker =
    .title = Weer te geven kolommen selecteren
tree-list-view-column-picker-restore =
    .label = Kolomvolgorde herstellen
tree-list-view-row-thread =
    .alt = Berichtconversatie-indicator
    .title = Dit is een bericht in een conversatie
tree-list-view-row-flagged =
    .alt = Bericht-met-ster-indicator
    .title = Bericht met ster
tree-list-view-row-flag =
    .alt = Bericht-met-ster-indicator
    .title = Bericht zonder ster
tree-list-view-row-attach =
    .alt = Bijlage-indicator
    .title = Bericht bevat bijlagen
tree-list-view-row-spam =
    .alt = Ongewenstberichtstatus-indicator
    .title = Bericht gemarkeerd als ongewenst
tree-list-view-row-not-spam =
    .alt = Ongewenstberichtstatus-indicator
    .title = Bericht niet gemarkeerd als ongewenst
