# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Şu anda tüm takvimler devre dışı. Etkinlik oluşturmak ve düzenlemek için bir takvimi etkinleştirin veya yeni takvim ekleyin.
calendar-deactivated-notification-tasks = Şu anda tüm takvimler devre dışı. Görev oluşturmak ve düzenlemek için bir takvimi etkinleştirin veya yeni takvim ekleyin.
calendar-notifications-label = Yaklaşan etkinlikler için bildirim göster
calendar-add-notification-button =
    .label = Bildirim ekle

## Side panel

calendar-list-header = Takvimler
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = { $calendarName } takvim seçeneği
calendar-import-new-calendar = Yeni takvim…
    .title = Yeni bir takvim oluştur veya takvime abone ol
calendar-refresh-calendars =
    .title = Tüm takvimleri yeniden yükle ve değişiklikleri eşitle
calendar-new-event-primary-button = Yeni etkinlik
calendar-new-task-primary-button = Yeni görev

## Calendar navigation

calendar-nav-button-prev-tooltip-day =
    .title = Önceki gün
    .accesskey = c
calendar-nav-button-prev-tooltip-week =
    .title = Önceki hafta
    .accesskey = c
calendar-nav-button-prev-tooltip-multiweek =
    .title = Önceki hafta
    .accesskey = c
calendar-nav-button-prev-tooltip-month =
    .title = Önceki ay
    .accesskey = c
calendar-nav-button-next-tooltip-day =
    .title = Sonraki gün
    .accesskey = r
calendar-nav-button-next-tooltip-week =
    .title = Sonraki hafta
    .accesskey = r
calendar-nav-button-next-tooltip-multiweek =
    .title = Sonraki hafta
    .accesskey = r
calendar-nav-button-next-tooltip-month =
    .title = Sonraki ay
    .accesskey = r
calendar-today-button-tooltip =
    .title = Bugüne git
calendar-view-toggle-day = Gün
    .title = Günlük gösterime geç
calendar-view-toggle-week = Hafta
    .title = Hafta görünümüne geç
calendar-view-toggle-multiweek = Çok haftalı
    .title = Çok haftalı görünüme geç
calendar-view-toggle-month = Ay
    .title = Aylık gösterime geç
