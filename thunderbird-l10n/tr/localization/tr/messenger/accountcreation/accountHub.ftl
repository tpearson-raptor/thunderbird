# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = <span data-l10n-name="brand-name">{ -brand-full-name }</span>’e hoş geldiniz
account-hub-title = Hesap merkezi

## Footer

account-hub-release-notes = Sürüm notları
account-hub-support = Destek
account-hub-donate = Bağış yapın

## Start page

account-hub-email-button = E-posta hesabı kur
account-hub-new-email-button = Yeni bir e-posta adresi al
account-hub-calendar-button = Takvimi ayarla
account-hub-address-book-button = Adres defterini ayarla
account-hub-chat-button = Sohbeti ayarla
account-hub-feed-button = RSS beslemesini ayarla
account-hub-newsgroup-button = Haber grubu kur
account-hub-import-button = Profili içe aktar
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Eşitlemek için giriş yap…

## Email page

account-hub-email-title = E-posta hesabınızı ayarlayın
account-hub-email-cancel-button = Vazgeç
account-hub-email-back-button = Geri
account-hub-email-continue-button = Devam et
account-hub-email-confirm-button = Onayla
