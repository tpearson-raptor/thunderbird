# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Geçerli satırı seçmek/seçimi kaldırmak için onay kutusu
    .title = Geçerli satırı seç
tree-list-view-row-deselect =
    .alt = Geçerli satırı seçmek/seçimi kaldırmak için onay kutusu
    .title = Geçerli satırın seçimini kaldır
tree-list-view-row-delete =
    .title = Geçerli satırı sil
tree-list-view-column-picker =
    .title = Gösterilecek sütunları seçin
tree-list-view-column-picker-restore =
    .label = Sütun sırasını sıfırla
tree-list-view-row-flagged =
    .alt = Yıldızlı ileti göstergesi
    .title = İleti yıldızlı
tree-list-view-row-flag =
    .alt = Yıldızlı ileti göstergesi
    .title = İleti yıldızlı değil
tree-list-view-row-attach =
    .alt = Ek göstergesi
    .title = İleti ek içeriyor
tree-list-view-row-spam =
    .alt = Gereksiz durumu göstergesi
    .title = İleti gereksiz olarak işaretlendi
tree-list-view-row-not-spam =
    .alt = Gereksiz durumu göstergesi
    .title = İleti gereksiz olarak işaretlenmedi
