# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = ปฏิทินทั้งหมดถูกปิดใช้งานในขณะนี้ ให้เปิดใช้งานปฏิทินที่มีอยู่หรือเพิ่มปฏิทินใหม่เพื่อสร้างและแก้ไขเหตุการณ์
calendar-deactivated-notification-tasks = ปฏิทินทั้งหมดถูกปิดใช้งานในขณะนี้ ให้เปิดใช้งานปฏิทินที่มีอยู่หรือเพิ่มปฏิทินใหม่เพื่อสร้างและแก้ไขงาน
calendar-notifications-label = แสดงการแจ้งเตือนสำหรับเหตุการณ์ที่กำลังจะเกิดขึ้น
calendar-add-notification-button =
    .label = เพิ่มการแจ้งเตือน
calendar-view-toggle-day = วัน
    .title = สลับเป็นมุมมองวัน
calendar-view-toggle-week = สัปดาห์
    .title = สลับเป็นมุมมองสัปดาห์
calendar-view-toggle-multiweek = หลายสัปดาห์
    .title = สลับเป็นมุมมองหลายสัปดาห์
calendar-view-toggle-month = เดือน
    .title = สลับเป็นมุมมองเดือน
