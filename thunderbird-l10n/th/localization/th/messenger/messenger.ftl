# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Window controls

about-rights-notification-text = { -brand-short-name } เป็นซอฟต์แวร์ฟรีและเปิดต้นฉบับที่สร้างโดยชุมชนที่มีนับพันคนจากทั่วทุกมุมโลก

## Content tabs


## Toolbar


## Folder Pane

folder-pane-toolbar =
    .toolbarname = แถบเครื่องมือบานหน้าต่างโฟลเดอร์
    .accesskey = ฟ
folder-pane-toolbar-options-button =
    .tooltiptext = ตัวเลือกบานหน้าต่างโฟลเดอร์
folder-pane-header-label = โฟลเดอร์

## Folder Toolbar Header Popup

folder-toolbar-hide-toolbar-toolbarbutton =
    .label = ซ่อนแถบเครื่องมือ
    .accesskey = ซ
show-all-folders-label =
    .label = โฟลเดอร์ทั้งหมด
    .accesskey = ท
show-unread-folders-label =
    .label = โฟลเดอร์ที่ยังไม่ได้อ่าน
    .accesskey = ม
show-favorite-folders-label =
    .label = โฟลเดอร์โปรด
    .accesskey = ป
show-smart-folders-label =
    .label = โฟลเดอร์แบบรวม
    .accesskey = ร
show-recent-folders-label =
    .label = โฟลเดอร์ล่าสุด
    .accesskey = ล
folder-toolbar-toggle-folder-compact-view =
    .label = มุมมองแบบกะทัดรัด
    .accesskey = ก

## Menu


## AppMenu


## Context menu


## Message header pane


## Message header cutomize panel


## Action Button Context Menu


## Add-on removal warning


## no-reply handling


## error messages


## Spaces toolbar


## Spaces toolbar pinned tab menupopup


## Spaces toolbar customize panel


## Quick Filter Bar

# The label to display for the "View... Toolbars..." menu item that controls
# whether the quick filter bar is visible.
quick-filter-bar-toggle =
    .label = แถบตัวกรองแบบรวดเร็ว
    .accesskey = ถ
# This is the key used to show the quick filter bar.
# This should match quick-filter-bar-textbox-shortcut in about3Pane.ftl.
quick-filter-bar-show =
    .key = k
