# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

about-addressbook-title = สมุดรายชื่อ

## Toolbar

about-addressbook-toolbar-new-address-book =
    .label = สมุดรายชื่อใหม่
about-addressbook-toolbar-new-contact =
    .label = ผู้ติดต่อใหม่
about-addressbook-toolbar-new-list =
    .label = รายชื่อใหม่

## Books

all-address-books = สมุดรายชื่อทั้งหมด
about-addressbook-books-context-synchronize =
    .label = ประสาน
about-addressbook-books-context-edit =
    .label = แก้ไข

## Cards


## Card column headers
## Each string is listed here twice, and the values should match.


## Card list placeholder
## Shown when there are no cards in the list


## Details


# Photo dialog


# Keyboard shortcuts

