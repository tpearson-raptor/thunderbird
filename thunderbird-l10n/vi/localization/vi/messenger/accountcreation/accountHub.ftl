# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Chào mừng đến với <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Trung tâm tài khoản

## Footer

account-hub-release-notes = Ghi chú phát hành
account-hub-support = Hỗ trợ
account-hub-donate = Quyên góp

## Start page

account-hub-email-button = Thiết lập tài khoản email
account-hub-new-email-button = Tạo một địa chỉ email mới
account-hub-calendar-button = Thiết lập lịch
account-hub-address-book-button = Thiết lập sổ địa chỉ
account-hub-chat-button = Thiết lập trò chuyện
account-hub-feed-button = Thiết lập nguồn cấp dữ liệu RSS
account-hub-newsgroup-button = Thiết lập nhóm tin
account-hub-import-button = Nhập hồ sơ
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Đăng nhập để đồng bộ hóa…

## Email page

account-hub-email-title = Thiết lập tài khoản email của bạn
account-hub-email-cancel-button = Huỷ bỏ
account-hub-email-back-button = Quay lại
account-hub-email-continue-button = Tiếp tục
account-hub-email-confirm-button = Xác nhận
