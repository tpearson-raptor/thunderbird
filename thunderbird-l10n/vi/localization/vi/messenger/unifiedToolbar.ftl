# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar strings


## Search bar

search-bar-button =
    .alt = Tìm kiếm
search-bar-item =
    .label = Tìm kiếm:
search-bar-placeholder = Tìm kiếm…

## Unified toolbar context menu

customize-menu-customize =
    .label = Tùy biến…

## Unified Toolbar customization

customize-title = Tùy biến thanh công cụ
customize-space-tab-mail = Thư
    .title = Thư
customize-space-tab-addressbook = Sổ địa chỉ
    .title = Sổ địa chỉ
customize-space-tab-calendar = Lịch
    .title = Lịch
customize-space-tab-tasks = Nhiệm vụ
    .title = Nhiệm vụ
customize-space-tab-chat = Trò chuyện
    .title = Trò chuyện
customize-space-tab-settings = Cài đặt
    .title = Cài đặt
customize-restore-default = Khôi phục về mặc định
customize-button-style-label = Kiểu nút:
customize-button-style-icons-beside-text =
    .label = Biểu tượng bên cạnh văn bản
customize-button-style-icons-above-text =
    .label = Biểu tượng trên văn bản
customize-button-style-icons-only =
    .label = Chỉ biểu tượng
customize-button-style-text-only =
    .label = Chỉ văn bản
customize-cancel = Hủy bỏ
customize-save = Lưu
customize-unsaved-changes = Các thay đổi chưa được lưu trong các không gian khác
customize-search-bar =
    .label = Các nút trên thanh công cụ tìm kiếm…
customize-main-toolbar-target =
    .aria-label = Thanh công cụ chính

## Unified toolbar customization palette context menu

# Variables:
# $target (String) - Name of the target the item should be added to.
customize-palette-add-to =
    .label = Thêm vào { $target }

## Unified toolbar customization target context menu

customize-target-remove =
    .label = Xóa
