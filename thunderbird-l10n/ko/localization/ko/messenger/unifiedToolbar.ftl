# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar strings


## Search bar


## Unified toolbar context menu

customize-menu-customize =
    .label = 사용자 정의…

## Unified Toolbar customization

customize-button-style-icons-beside-text =
    .label = 텍스트 옆에 아이콘

## Unified toolbar customization palette context menu


## Unified toolbar customization target context menu

