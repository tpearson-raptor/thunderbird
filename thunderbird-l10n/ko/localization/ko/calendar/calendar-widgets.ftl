# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = 모든 캘린더가 현재 비활성화되어 있습니다. 기존 캘린더를 활성화하거나 새 캘린더를 추가하여 이벤트 생성 및 편집을 하십시오.
calendar-deactivated-notification-tasks = 모든 캘린더가 현재 비활성화되어 있습니다. 기존 캘린더를 활성화하거나 새 캘린더를 추가하여 작업 생성 및 편집을 하십시오.
calendar-notifications-label = 예정된 이벤트에 대한 알림 표시
calendar-add-notification-button =
    .label = 알림 추가
calendar-view-toggle-day = 일별
    .title = 일별 보기로 전환합니다
calendar-view-toggle-week = 주별
    .title = 주별 보기로 전환합니다
calendar-view-toggle-multiweek = 여러 주
    .title = 여러 주 보기로 전환합니다
calendar-view-toggle-month = 월별
    .title = 월별 보기로 전환합니다
