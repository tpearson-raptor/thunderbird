# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Jelenleg az összes naptár le van tiltva. Engedélyezzen egy meglévő naptárt, vagy adjon hozzá egy újat az események létrehozásához és szerkesztéséhez.
calendar-deactivated-notification-tasks = Jelenleg az összes naptár le van tiltva. Engedélyezzen egy meglévő naptárt, vagy adjon hozzá egy újat a feladatok létrehozásához és szerkesztéséhez.
calendar-notifications-label = Közelgő események értesítéseinek megtekintése
calendar-add-notification-button =
    .label = Értesítés hozzáadása

## Side panel

calendar-list-header = Naptárak
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = A(z) „{ $calendarName }” naptár beállításai
calendar-import-new-calendar = Új naptár…
    .title = Új naptár létrehozása vagy feliratkozás
calendar-refresh-calendars =
    .title = Az összes naptár újratöltése és a módosítások szinkronizálása
calendar-new-event-primary-button = Új esemény
calendar-new-task-primary-button = Új feladat

## Calendar navigation

calendar-nav-button-prev-tooltip-day =
    .title = Előző nap
    .accesskey = n
calendar-nav-button-prev-tooltip-week =
    .title = Előző hét
    .accesskey = h
calendar-nav-button-prev-tooltip-multiweek =
    .title = Előző hét
    .accesskey = h
calendar-nav-button-prev-tooltip-month =
    .title = Előző hónap
    .accesskey = h
calendar-nav-button-next-tooltip-day =
    .title = Következő nap
    .accesskey = K
calendar-nav-button-next-tooltip-week =
    .title = Következő hét
    .accesskey = v
calendar-nav-button-next-tooltip-multiweek =
    .title = Következő hét
    .accesskey = v
calendar-nav-button-next-tooltip-month =
    .title = Következő hónap
    .accesskey = p
calendar-today-button-tooltip =
    .title = Ugrás a mai napra
calendar-view-toggle-day = Nap
    .title = Átváltás napi nézetre
calendar-view-toggle-week = Hét
    .title = Átváltás heti nézetre
calendar-view-toggle-multiweek = Több hét
    .title = Átváltás többheti nézetre
calendar-view-toggle-month = Hónap
    .title = Átváltás havi nézetre
