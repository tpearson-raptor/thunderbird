# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Üdvözli a <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Fiókközpont

## Footer

account-hub-release-notes = Kiadási megjegyzések
account-hub-support = Támogatás
account-hub-donate = Adományozás

## Start page

account-hub-email-button = E-mail-fiók beállítása
account-hub-new-email-button = Új e-mail-cím kérése
account-hub-calendar-button = Naptár létrehozása
account-hub-address-book-button = Címjegyzék létrehozása
account-hub-chat-button = Csevegés létrehozása
account-hub-feed-button = RSS-hírcsatorna létrehozása
account-hub-newsgroup-button = Hírcsoport létrehozása
account-hub-import-button = Profil importálása
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Jelentkezzen be a Syncbe…

## Email page

account-hub-email-title = Állítsa be e-mail-fiókját
account-hub-email-cancel-button = Mégse
account-hub-email-back-button = Vissza
account-hub-email-continue-button = Folytatás
account-hub-email-confirm-button = Megerősítés
