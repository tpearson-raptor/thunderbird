# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Jelelőmező a jelenlegi sor kiválasztásához
    .title = A jelenlegi sor kiválasztása
tree-list-view-row-deselect =
    .alt = Jelelőmező a jelenlegi sor kiválasztásához
    .title = A jelenlegi sor kiválasztásának megszüntetése
tree-list-view-row-delete =
    .title = A jelenlegi sor törlése
tree-list-view-column-picker =
    .title = Jelölje ki a megjelenítendő oszlopokat
tree-list-view-column-picker-restore =
    .label = Oszlopsorrend visszaállítása
tree-list-view-row-thread =
    .alt = Üzenetszálban lévő üzenet jelzője
    .title = Ez egy üzenetszálban lévő üzenet
tree-list-view-row-flagged =
    .alt = Csillagozott üzenet jelzője
    .title = Az üzenet csillagozott
tree-list-view-row-flag =
    .alt = Csillagozott üzenet jelzője
    .title = Az üzenet nem csillagozott
tree-list-view-row-attach =
    .alt = Mellékletjelölő
    .title = Az üzenet mellékleteket tartalmaz
tree-list-view-row-spam =
    .alt = Kéretlen tartalom jelzője
    .title = Az üzenet kéretlenként lett megjelölve
tree-list-view-row-not-spam =
    .alt = Kéretlen tartalom jelzője
    .title = Az üzenet nincs kéretlenként megjelölve
