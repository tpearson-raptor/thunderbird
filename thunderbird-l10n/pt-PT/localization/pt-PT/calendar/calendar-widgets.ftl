# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Todos os calendários estão neste momento desativados. Ative um calendário existente ou adicione um novo, para criar e editar eventos.
calendar-deactivated-notification-tasks = Todos os calendários estão neste momento desativados. Ative um calendário existente ou adicione um novo, para criar e editar tarefas.
calendar-notifications-label = Mostrar notificações de próximos eventos
calendar-add-notification-button =
    .label = Adicionar notificação
calendar-view-toggle-day = Dia
    .title = Mudar para vista diária
calendar-view-toggle-week = Semana
    .title = Mudar para vista semanal
calendar-view-toggle-multiweek = Multissemana
    .title = Mudar para vista multissemanal
calendar-view-toggle-month = Mês
    .title = Mudar para vista mensal
