# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Sync


## New Account


## New Account / Address Book


## Create


## Open


## View / Layout


## Tools


## Help

appmenu-help-enter-troubleshoot-mode2 =
    .label = Modo de Diagnóstico...
    .accesskey = M
appmenu-help-exit-troubleshoot-mode2 =
    .label = Desligar o modo de diagnóstico
    .accesskey = D
appmenu-help-troubleshooting-info =
    .label = Informação da Resolução de Problemas
    .accesskey = I

## Application Update

