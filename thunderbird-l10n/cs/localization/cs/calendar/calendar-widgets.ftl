# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Všechny vaše kalendáře jsou v tuto chvíli zakázané. Pro vytváření a úpravu událostí povolte nějaký stávající kalendář nebo vytvořte nový.
calendar-deactivated-notification-tasks = Všechny vaše kalendáře jsou v tuto chvíli zakázané. Pro vytváření a úpravu úkolů povolte nějaký stávající kalendář nebo vytvořte nový.
calendar-notifications-label = Zobrazit oznámení pro nadcházející události
calendar-add-notification-button =
    .label = Přidat oznámení
calendar-view-toggle-day = Den
    .title = Přepne do denního pohledu
calendar-view-toggle-week = Týden
    .title = Přepne do týdenního pohledu
calendar-view-toggle-multiweek = Vícetýden
    .title = Přepne do vícetýdenního pohledu
calendar-view-toggle-month = Měsíc
    .title = Přepne do měsíčního pohledu
