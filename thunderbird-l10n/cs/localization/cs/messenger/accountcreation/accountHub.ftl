# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Vítá vás <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Účty

## Footer

account-hub-release-notes = Poznámky k vydání
account-hub-support = Podpora
account-hub-donate = Přispějte

## Start page


## Email page

account-hub-email-cancel-button = Zrušit
account-hub-email-back-button = Zpět
account-hub-email-continue-button = Pokračovat
account-hub-email-confirm-button = Potvrdit
