# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar strings


## Search bar


## Unified toolbar context menu

customize-menu-customize =
    .label = Přizpůsobit…

## Unified Toolbar customization

customize-space-tab-mail = Pošta
    .title = Pošta
customize-space-tab-addressbook = Kontakty
    .title = Kontakty
customize-space-tab-calendar = Kalendář
    .title = Kalendář
customize-space-tab-tasks = Úkoly
    .title = Úkoly
customize-space-tab-chat = Chat
    .title = Chat
customize-space-tab-settings = Nastavení
    .title = Nastavení
customize-button-style-icons-beside-text =
    .label = Ikony vedle textu

## Unified toolbar customization palette context menu


## Unified toolbar customization target context menu

