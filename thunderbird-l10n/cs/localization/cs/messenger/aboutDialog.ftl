# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-update-whats-new = Co je nového
aboutDialog-title =
    .title =
        { -brand-full-name.case-status ->
            [with-cases] O { -brand-full-name(case: "loc") }
           *[no-cases] O aplikaci { -brand-full-name }
        }
