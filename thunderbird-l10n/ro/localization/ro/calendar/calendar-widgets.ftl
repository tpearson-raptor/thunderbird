# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Toate calendarele sunt acum dezactivate. Activează un calendar existent sau adaugă unul nou pentru a crea și edita evenimente.
calendar-deactivated-notification-tasks = Toate calendarele sunt acum dezactivate. Activează un calendar existent sau adaugă unul nou pentru a crea și edita sarcini.
calendar-view-toggle-day = Zi
    .title = Comută pe vizualizarea pe zile
calendar-view-toggle-week = Săptămână
    .title = Comută pe vizualizarea pe săptămâni
calendar-view-toggle-multiweek = Multisăptămânal
    .title = Comută pe vizualizarea pe mai multe săptămâni
calendar-view-toggle-month = Lună
    .title = Comută pe vizualizarea pe luni
