# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Alla kalendrar är för närvarande inaktiverade. Aktivera en befintlig kalender eller lägg till en ny för att skapa och redigera händelser.
calendar-deactivated-notification-tasks = Alla kalendrar är för närvarande inaktiverade. Aktivera en befintlig kalender eller lägg till en ny för att skapa och redigera uppgifter.
calendar-notifications-label = Visa aviseringar för kommande händelser
calendar-add-notification-button =
    .label = Lägg till avisering

## Side panel

calendar-list-header = Kalendrar
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = { $calendarName } kalenderalternativ
calendar-import-new-calendar = Ny kalender…
    .title = Skapa eller prenumerera på en ny kalender
calendar-refresh-calendars =
    .title = Ladda om alla kalendrar och synkronisera ändringar
calendar-new-event-primary-button = Ny händelse
calendar-new-task-primary-button = Ny uppgift

## Calendar navigation

calendar-nav-button-prev-tooltip-day =
    .title = Föregående dag
    .accesskey = F
calendar-nav-button-prev-tooltip-week =
    .title = Föregående vecka
    .accesskey = F
calendar-nav-button-prev-tooltip-multiweek =
    .title = Föregående vecka
    .accesskey = F
calendar-nav-button-prev-tooltip-month =
    .title = Föregående månad
    .accesskey = F
calendar-nav-button-next-tooltip-day =
    .title = Nästa dag
    .accesskey = N
calendar-nav-button-next-tooltip-week =
    .title = Nästa vecka
    .accesskey = N
calendar-nav-button-next-tooltip-multiweek =
    .title = Nästa vecka
    .accesskey = N
calendar-nav-button-next-tooltip-month =
    .title = Nästa månad
    .accesskey = N
calendar-today-button-tooltip =
    .title = Gå till idag
calendar-view-toggle-day = Dag
    .title = Växla till dagsvy
calendar-view-toggle-week = Vecka
    .title = Växla till veckovy
calendar-view-toggle-multiweek = Flera veckor
    .title = Växla till flerveckorsöversikt
calendar-view-toggle-month = Månad
    .title = Växla till månadsvy
