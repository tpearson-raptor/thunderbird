# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Välkommen till <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Kontocenter

## Footer

account-hub-release-notes = Versionsfakta
account-hub-support = Support
account-hub-donate = Donera

## Start page

account-hub-email-button = Konfigurera e-postkonto
account-hub-new-email-button = Skapa en ny e-postadress
account-hub-calendar-button = Ställ in en kalender
account-hub-address-book-button = Ställ in adressboken
account-hub-chat-button = Ställ in chatt
account-hub-feed-button = Ställ in RSS-flöde
account-hub-newsgroup-button = Ställ in nyhetsgrupp
account-hub-import-button = Importera profil
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Logga in för att synkronisera…

## Email page

account-hub-email-title = Konfigurera ditt e-postkonto
account-hub-email-cancel-button = Avbryt
account-hub-email-back-button = Tillbaka
account-hub-email-continue-button = Fortsätt
account-hub-email-confirm-button = Bekräfta
