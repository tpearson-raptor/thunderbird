# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Kryssrutan för att markera den aktuella raden
    .title = Markera den aktuella raden
tree-list-view-row-deselect =
    .alt = Kryssrutan för att markera den aktuella raden
    .title = Avmarkera den aktuella raden
tree-list-view-row-delete =
    .title = Ta bort den aktuella raden
tree-list-view-column-picker =
    .title = Välj kolumner att visa
tree-list-view-column-picker-restore =
    .label = Återställ kolumnordning
tree-list-view-row-thread =
    .alt = Indikator för meddelandetråd
    .title = Detta är ett trådat meddelande
tree-list-view-row-flagged =
    .alt = Indikator för stjärnmärkt meddelande
    .title = Meddelandet stjärnmärkt
tree-list-view-row-flag =
    .alt = Indikator för stjärnmärkt meddelande
    .title = Meddelandet inte stjärnmärkt
tree-list-view-row-attach =
    .alt = Indikator för bilagor
    .title = Meddelandet innehåller bilagor
tree-list-view-row-spam =
    .alt = Indikator för skräppoststatus
    .title = Meddelande markerat som skräppost
tree-list-view-row-not-spam =
    .alt = Indikator för skräppoststatus
    .title = Meddelande inte markerat som skräppost
