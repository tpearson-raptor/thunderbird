# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Wszystkie kalendarze są obecnie wyłączone. Włącz istniejący kalendarz lub dodaj nowy, aby tworzyć i modyfikować wydarzenia.
calendar-deactivated-notification-tasks = Wszystkie kalendarze są obecnie wyłączone. Włącz istniejący kalendarz lub dodaj nowy, aby tworzyć i modyfikować zadania.
calendar-notifications-label = Wyświetlaj powiadomienia o nadchodzących wydarzeniach
calendar-add-notification-button =
    .label = Dodaj powiadomienie
calendar-import-new-calendar = Nowy kalendarz…
    .title = Utwórz lub subskrybuj nowy kalendarz
calendar-refresh-calendars =
    .title = Odśwież wszystkie kalendarze i synchronizuj zmiany
calendar-new-event-primary-button = Nowe wydarzenie
calendar-new-task-primary-button = Nowe zadanie
calendar-view-toggle-day = Dzień
    .title = Przełącz na widok dnia
calendar-view-toggle-week = Tydzień
    .title = Przełącz na widok tygodnia
calendar-view-toggle-multiweek = Wiele tygodni
    .title = Przełącz na widok wielu tygodni
calendar-view-toggle-month = Miesiąc
    .title = Przełącz na widok miesiąca
