# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Witamy w programie <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Centrum kont

## Footer

account-hub-release-notes = Informacje o wydaniu
account-hub-support = Pomoc
account-hub-donate = Przekaż datek

## Start page

account-hub-email-button = Skonfiguruj konto pocztowe
account-hub-new-email-button = Nowy adres e-mail
account-hub-calendar-button = Skonfiguruj kalendarz
account-hub-address-book-button = Skonfiguruj książkę adresową
account-hub-chat-button = Skonfiguruj komunikator
account-hub-feed-button = Skonfiguruj źródło aktualności RSS
account-hub-newsgroup-button = Skonfiguruj grupę dyskusyjną
account-hub-import-button = Importuj profil
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Zaloguj się do synchronizacji…

## Email page

account-hub-email-title = Skonfiguruj konto pocztowe
account-hub-email-cancel-button = Anuluj
account-hub-email-back-button = Wstecz
account-hub-email-continue-button = Kontynuuj
account-hub-email-confirm-button = Potwierdź
