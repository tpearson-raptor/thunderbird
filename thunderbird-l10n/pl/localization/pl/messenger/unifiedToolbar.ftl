# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar strings


## Search bar


## Unified toolbar context menu

customize-menu-customize =
    .label = Dostosuj…

## Unified Toolbar customization

customize-space-tab-mail = Poczta
    .title = Poczta
customize-space-tab-addressbook = Książka adresowa
    .title = Książka adresowa
customize-space-tab-calendar = Kalendarz
    .title = Kalendarz
customize-space-tab-tasks = Zadania
    .title = Zadania
customize-space-tab-chat = Komunikator
    .title = Komunikator
customize-space-tab-settings = Ustawienia
    .title = Ustawienia
customize-button-style-icons-beside-text =
    .label = Ikony obok tekstu

## Unified toolbar customization palette context menu


## Unified toolbar customization target context menu

