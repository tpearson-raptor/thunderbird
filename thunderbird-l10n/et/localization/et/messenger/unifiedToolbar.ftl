# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar strings


## Search bar


## Unified toolbar context menu

customize-menu-customize =
    .label = Kohanda...

## Unified Toolbar customization

customize-space-tab-mail = E-post
    .title = E-post
customize-space-tab-addressbook = Aadressiraamat
    .title = Aadressiraamat
customize-space-tab-calendar = Kalender
    .title = Kalender
customize-space-tab-tasks = Ülesanded
    .title = Ülesanded
customize-space-tab-chat = Kiirsuhtlus
    .title = Kiirsuhtlus
customize-space-tab-settings = Sätted
    .title = Sätted
customize-button-style-icons-beside-text =
    .label = Ikoonid teksti kõrval

## Unified toolbar customization palette context menu


## Unified toolbar customization target context menu

