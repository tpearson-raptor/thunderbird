# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Bienvenue dans <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Centre de compte

## Footer

account-hub-release-notes = Notes de version
account-hub-support = Assistance
account-hub-donate = Faire un don

## Start page

account-hub-email-button = Configurer un compte de courrier
account-hub-new-email-button = Obtenir une nouvelle adresse électronique
account-hub-calendar-button = Configurer un agenda
account-hub-address-book-button = Configurer un carnet d’adresses
account-hub-chat-button = Configurer la messagerie instantanée
account-hub-feed-button = Configurer un flux RSS
account-hub-newsgroup-button = Configurer un groupe de discussion
account-hub-import-button = Importer un profil
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Se connecter pour synchroniser…

## Email page

account-hub-email-title = Configurer un compte de courrier électronique
account-hub-email-cancel-button = Annuler
account-hub-email-back-button = Retour
account-hub-email-continue-button = Continuer
account-hub-email-confirm-button = Confirmer
