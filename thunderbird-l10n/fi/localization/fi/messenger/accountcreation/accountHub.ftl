# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Tässä <span data-l10n-name="brand-name">{ -brand-full-name }</span>, tervetuloa
account-hub-title = Tilikeskus

## Footer

account-hub-release-notes = Julkaisutiedot
account-hub-support = Tuki
account-hub-donate = Lahjoita

## Start page

account-hub-email-button = Määritä sähköpostitili
account-hub-new-email-button = Hanki uusi sähköpostiosoite
account-hub-calendar-button = Määritä kalenteri
account-hub-address-book-button = Määritä osoitekirja
account-hub-chat-button = Määritä keskustelu
account-hub-feed-button = Määritä RSS-syöte
account-hub-import-button = Tuo profiili

## Email page

account-hub-email-title = Määritä sähköpostitilisi
account-hub-email-cancel-button = Peruuta
account-hub-email-back-button = Edellinen
account-hub-email-continue-button = Jatka
account-hub-email-confirm-button = Vahvista
