# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-view-toggle-day = Часове
    .title = Превключване към дневен изглед
calendar-view-toggle-week = Дни
    .title = Превключване към седмичен излгед
calendar-view-toggle-multiweek = Седмици
    .title = Превключване към многоседмичен изглед
calendar-view-toggle-month = Месец
    .title = Превключване към месечен изглед
