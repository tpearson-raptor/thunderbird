# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Todos los calendarios están deshabilitados en este momento. Habilita un calendario existente o agrega uno nuevo para crear y editar eventos.
calendar-deactivated-notification-tasks = Todos los calendarios están deshabilitados en este momento. Habilita un calendario existente o agrega uno nuevo para crear y editar tareas.
calendar-notifications-label = Mostrar notificaciones para eventos futuros
calendar-add-notification-button =
    .label = Agregar notificación

## Side panel

calendar-list-header = Calendarios
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = { $calendarName } opción de calendario
calendar-import-new-calendar = Nuevo calendario…
    .title = Crear o suscribirte a un nuevo calendario
calendar-refresh-calendars =
    .title = Recargar todos los calendarios y sincronizar cambios
calendar-new-event-primary-button = Nuevo evento
calendar-new-task-primary-button = Nueva tarea

## Calendar navigation

calendar-today-button-tooltip =
    .title = Ir al día de hoy
calendar-view-toggle-day = Día
    .title = Cambiar a vista por día
calendar-view-toggle-week = Semana
    .title = Cambiar a vista por semana
calendar-view-toggle-multiweek = Multisemana
    .title = Cambiar a vista multisemana
calendar-view-toggle-month = Mes
    .title = Cambiar a vista por mes
