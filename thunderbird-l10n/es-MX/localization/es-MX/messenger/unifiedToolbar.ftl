# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar strings


## Search bar

search-bar-button =
    .alt = Buscar
search-bar-item =
    .label = Buscar:
search-bar-placeholder = Buscar…

## Unified toolbar context menu

customize-menu-customize =
    .label = Personalizar…

## Unified Toolbar customization

customize-title = Personalizar barras de herramientas
customize-space-tab-mail = Correo
    .title = Correo
customize-space-tab-addressbook = Libreta de direcciones
    .title = Libreta de direcciones
customize-space-tab-calendar = Calendario
    .title = Calendario
customize-space-tab-tasks = Tareas
    .title = Tareas
customize-space-tab-chat = Chat
    .title = Chat
customize-space-tab-settings = Ajustes
    .title = Ajustes
customize-restore-default = Restaurar predeterminado
customize-change-appearance = Cambiar apariencia…
customize-button-style-label = Estilo de botón:
customize-button-style-icons-beside-text =
    .label = Íconos a un lado del texto
customize-button-style-icons-only =
    .label = Solo iconos
customize-button-style-text-only =
    .label = Solo texto
customize-cancel = Cancelar
customize-save = Guardar
customize-spaces-tabs =
    .aria-label = Espacios
customize-main-toolbar-target =
    .aria-label = Barra de herramientas principal

## Unified toolbar customization palette context menu

# Variables:
# $target (String) - Name of the target the item should be added to.
customize-palette-add-to =
    .label = Agregar a { $target }

## Unified toolbar customization target context menu

customize-target-remove =
    .label = Eliminar
