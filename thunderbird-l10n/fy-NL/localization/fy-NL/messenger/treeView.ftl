# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Oanfinkfjild om de seleksje fan de aktuele rige te wiskeljen
    .title = De aktuele rige selektearje
tree-list-view-row-deselect =
    .alt = Oanfinkfjild om de seleksje fan de aktuele rige te wiskeljen
    .title = De aktuele rige deselektearje
tree-list-view-row-delete =
    .title = De aktuele rige fuortsmite
tree-list-view-column-picker =
    .title = Wer te jaan kolommen selektearje
tree-list-view-column-picker-restore =
    .label = Kolomfolchoarder tebeksette
tree-list-view-row-thread =
    .alt = Berjochtpetear-yndikator
    .title = Dit is in berjocht yn in petear
tree-list-view-row-flagged =
    .alt = Berjocht-mei-stjer-yndikator
    .title = Berjocht mei stjer
tree-list-view-row-flag =
    .alt = Berjocht-mei-stjer-yndikator
    .title = Berjocht sûnder stjer
tree-list-view-row-attach =
    .alt = Bylage-yndikator
    .title = Berjocht befettet bylagen
tree-list-view-row-spam =
    .alt = Net-winske-berjochtsteat-yndikator
    .title = Berjocht markearre as net-winske
tree-list-view-row-not-spam =
    .alt = Net-winske-berjochtsteat-yndikator
    .title = Berjocht net markearre as net-winske
