# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Wolkom by <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Accounthub

## Footer

account-hub-release-notes = Utjefteopmerkingen
account-hub-support = Stipe
account-hub-donate = Donearje

## Start page

account-hub-email-button = E-mailaccount ynstelle
account-hub-new-email-button = In nij e-mailadres oanfreegje
account-hub-calendar-button = Aginda ynstelle
account-hub-address-book-button = Adresboek ynstelle
account-hub-chat-button = Chat ynstelle
account-hub-feed-button = RSS-feed ynstelle
account-hub-newsgroup-button = Nijsgroep ynstelle
account-hub-import-button = Profyl ymportearje
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Oanmelde by Syngronisaasje…

## Email page

account-hub-email-title = Jo e-mailaccount ynstelle
account-hub-email-cancel-button = Annulearje
account-hub-email-back-button = Tebek
account-hub-email-continue-button = Trochgean
account-hub-email-confirm-button = Befêstigje
