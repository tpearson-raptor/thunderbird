# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar Item Label strings

spacer-label = Эластычны інтэрвал
toolbar-write-message-label = Укласці
toolbar-write-message =
    .title = Стварыць новы ліст
toolbar-folder-location-label = Месцазнаходжанне папкі
toolbar-get-messages-label = Атрымаць лісты
toolbar-reply-label = Адказаць
toolbar-reply =
    .title = Адказаць на ліст
toolbar-reply-all-label = Адк. усім
toolbar-reply-all =
    .title = Адказаць адпраўніку і ўсім атрымальнікам
toolbar-reply-to-list-label = Адказаць спісу
toolbar-reply-to-list =
    .title = Адказаць паштоваму спісу
toolbar-archive-label = Архіў
toolbar-archive =
    .title = Заархівіраваць вылучаныя лісты
toolbar-conversation-label = Размова
toolbar-previous-unread-label = Папярэдні непрачытаны
toolbar-previous-unread =
    .title = Перамясціцца да папярэдняга непрачытанага ліста
toolbar-previous-label = Папярэдні
toolbar-previous =
    .title = Перайсці да папаярэдняга ліста
toolbar-next-unread-label = Наступны непрачытаны
toolbar-next-unread =
    .title = Перамясціцца да наступнага непрачытанага ліста
toolbar-next-label = Наступны
toolbar-next =
    .title = Перайсці да наступнага ліста
toolbar-junk-label = Лухта
toolbar-junk =
    .title = Адзначыць выбраныя паведамленні як лухта
toolbar-delete-label = Выдаліць
toolbar-delete =
    .title = Выдаліць абраныя лісты ці тэчкі
toolbar-compact-label = Ушчыльніць
toolbar-compact =
    .title = Прыняць выдаленыя лісты з вылучанай папкі
toolbar-tag-message-label = Метка
toolbar-tag-message =
    .title = Меткі лістоў
toolbar-forward-inline-label = Накіраваць
toolbar-forward-inline =
    .title = Накіраваць вылучаны ліст як убудаваны тэкст
toolbar-forward-attachment-label = Накіраваць як далучэнне
toolbar-forward-attachment =
    .title = Накіраваць вылучаны ліст як далучэнне
toolbar-mark-as-label = Пазначыць
toolbar-mark-as =
    .title = Пазначыць лісты
toolbar-address-book =
    .title = Перайсці ў адрасную кнігу
toolbar-chat-label = Гутарка
toolbar-chat =
    .title = Паказаць устаўку гутаркі
toolbar-print-label = Друкаваць
toolbar-print =
    .title = Надрукаваць гэты ліст
