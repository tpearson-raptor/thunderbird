# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-view-toggle-day = Día
    .title = Cambiar á vista diaria
calendar-view-toggle-week = Semana
    .title = Cambiar á vista semanal
calendar-view-toggle-multiweek = Varias semanas
    .title = Cambiar á vista de varias semanas
calendar-view-toggle-month = Mes
    .title = Cambiar á vista mensual
