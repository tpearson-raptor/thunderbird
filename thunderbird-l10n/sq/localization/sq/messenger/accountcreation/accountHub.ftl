# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Mirë se vini te <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Qendër Llogarish

## Footer

account-hub-release-notes = Shënime hedhjeje në qarkullim
account-hub-support = Asistencë
account-hub-donate = Dhuroni

## Start page

account-hub-email-button = Ujdisni një llogari email
account-hub-new-email-button = Merrni një adresë të re email
account-hub-calendar-button = Ujdisni kalendar
account-hub-address-book-button = Ujdisni libër adresash
account-hub-chat-button = Ujdisni fjalosje
account-hub-feed-button = Ujdisni prurje RSS
account-hub-newsgroup-button = Ujdisni grup lajmesh
account-hub-import-button = Importoni profil
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Hyni në Sync…

## Email page

account-hub-email-title = Ujdisni llogarinë tuaj email
account-hub-email-cancel-button = Anuloje
account-hub-email-back-button = Mbrapsht
account-hub-email-continue-button = Vazhdo
account-hub-email-confirm-button = Ripohojeni
