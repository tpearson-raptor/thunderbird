# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Casilla para alternar la selección de la fila actual
    .title = Seleccionar la fila actual
tree-list-view-row-deselect =
    .alt = Casilla para alternar la selección de la fila actual
    .title = Deseleccionar la fila actual
tree-list-view-row-delete =
    .title = Borrar la fila actual
tree-list-view-column-picker =
    .title = Seleccionar las columnas a mostrar
tree-list-view-column-picker-restore =
    .label = Restaurar el orden de las columnas
tree-list-view-row-thread =
    .alt = Indicador de hilo de mensajes
    .title = Este es un mensaje en hilo
tree-list-view-row-flagged =
    .alt = Indicador de mensaje destacado
    .title = Mensaje destacado
tree-list-view-row-flag =
    .alt = Indicador de mensaje destacado
    .title = Mensaje no destacado
tree-list-view-row-attach =
    .alt = Indicador de adjunto
    .title = El mensaje contiene adjuntos
tree-list-view-row-spam =
    .alt = Indicador de estado de spam
    .title = Mensaje marcado como spam
tree-list-view-row-not-spam =
    .alt = Indicador de estado de spam
    .title = Mensaje no marcado como spam
