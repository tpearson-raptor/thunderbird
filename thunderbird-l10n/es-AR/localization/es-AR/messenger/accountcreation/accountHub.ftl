# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Bienvenido a <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Centro de cuentas

## Footer

account-hub-release-notes = Notas de la versión
account-hub-support = Ayuda
account-hub-donate = Donar

## Start page

account-hub-email-button = Configurar una cuenta de correo electrónico
account-hub-new-email-button = Obtener una nueva dirección de correo electrónico
account-hub-calendar-button = Configurar calendario
account-hub-address-book-button = Configurar libreta de direcciones
account-hub-chat-button = Configurar chat
account-hub-feed-button = Configurar fuente RSS
account-hub-newsgroup-button = Configurar grupo de noticias
account-hub-import-button = Importar perfil
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Ingresar a Sync…

## Email page

account-hub-email-title = Configurar una cuenta de correo electrónico
account-hub-email-cancel-button = Cancelar
account-hub-email-back-button = Atrás
account-hub-email-continue-button = Continuar
account-hub-email-confirm-button = Confirmar
