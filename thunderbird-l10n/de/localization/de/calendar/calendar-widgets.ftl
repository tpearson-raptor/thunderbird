# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Derzeit sind alle Kalender deaktiviert. Aktivieren Sie einen bestehenden Kalender oder fügen Sie einen neuen hinzu, um Termine zu erstellen und bearbeiten.
calendar-deactivated-notification-tasks = Derzeit sind alle Kalender deaktiviert. Aktivieren Sie einen bestehenden Kalender oder fügen Sie einen neuen hinzu, um Aufgaben zu erstellen und bearbeiten.
calendar-notifications-label = Benachrichtigungen für demnächst anstehende Termine anzeigen
calendar-add-notification-button =
    .label = Benachrichtigung hinzufügen
calendar-view-toggle-day = Tag
    .title = Zur Tagesansicht wechseln
calendar-view-toggle-week = Woche
    .title = Zur Wochenansicht wechseln
calendar-view-toggle-multiweek = Mehrere Wochen
    .title = Zur mehrwöchigen Ansicht wechseln
calendar-view-toggle-month = Monat
    .title = Zur Monatsansicht wechseln
