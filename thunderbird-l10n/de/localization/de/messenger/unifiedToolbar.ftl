# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar strings


## Search bar


## Unified toolbar context menu

customize-menu-customize =
    .label = Anpassen…

## Unified Toolbar customization

customize-space-tab-mail = E-Mail
    .title = E-Mail
customize-space-tab-addressbook = Adressbuch
    .title = Adressbuch
customize-space-tab-calendar = Kalender
    .title = Kalender
customize-space-tab-tasks = Aufgaben
    .title = Aufgaben
customize-space-tab-chat = Chat
    .title = Chat
customize-space-tab-settings = Einstellungen
    .title = Einstellungen
customize-button-style-icons-beside-text =
    .label = Symbole neben Text

## Unified toolbar customization palette context menu


## Unified toolbar customization target context menu

