# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Vsi koledarji so trenutno onemogočeni. Za ustvarjanje in urejanje dogodkov omogočite obstoječi koledar ali dodajte novega.
calendar-deactivated-notification-tasks = Vsi koledarji so trenutno onemogočeni. Za ustvarjanje in urejanje opravil omogočite obstoječi koledar ali dodajte novega.
calendar-notifications-label = Prikazuj obvestila o prihajajočih dogodkih
calendar-add-notification-button =
    .label = Dodaj obvestilo

## Side panel

calendar-list-header = Koledarji
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = Možnost koledarja { $calendarName }
calendar-import-new-calendar = Nov koledar …
    .title = Ustvarite ali se naročite na nov koledar
calendar-refresh-calendars =
    .title = Ponovno naloži vse koledarje in sinhroniziraj spremembe
calendar-new-event-primary-button = Nov dogodek
calendar-new-task-primary-button = Novo opravilo

## Calendar navigation

calendar-nav-button-prev-tooltip-day =
    .title = Prejšnji dan
    .accesskey = P
calendar-nav-button-prev-tooltip-week =
    .title = Prejšnji teden
    .accesskey = P
calendar-nav-button-prev-tooltip-multiweek =
    .title = Prejšnji teden
    .accesskey = P
calendar-nav-button-prev-tooltip-month =
    .title = Prejšnji mesec
    .accesskey = P
calendar-nav-button-next-tooltip-day =
    .title = Naslednji dan
    .accesskey = N
calendar-nav-button-next-tooltip-week =
    .title = Naslednji teden
    .accesskey = N
calendar-nav-button-next-tooltip-multiweek =
    .title = Naslednji teden
    .accesskey = N
calendar-nav-button-next-tooltip-month =
    .title = Naslednji mesec
    .accesskey = N
calendar-today-button-tooltip =
    .title = Pojdi na današnji dan
calendar-view-toggle-day = Dan
    .title = Preklopi na dnevni pogled
calendar-view-toggle-week = Teden
    .title = Preklopi na tedenski pogled
calendar-view-toggle-multiweek = Več tednov
    .title = Preklopi na večtedenski pogled
calendar-view-toggle-month = Mesec
    .title = Preklopi na mesečni pogled
