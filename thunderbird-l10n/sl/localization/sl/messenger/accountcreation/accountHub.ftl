# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Dobrodošli v <span data-l10n-name="brand-name">{ -brand-full-name(sklon: "mestnik") }</span>
account-hub-title = Središče za račune

## Footer

account-hub-release-notes = Opombe ob izdaji
account-hub-support = Podpora
account-hub-donate = Donirajte

## Start page

account-hub-email-button = Nastavi račun za e-pošto
account-hub-new-email-button = Pridobi nov e-poštni naslov
account-hub-calendar-button = Nastavi koledar
account-hub-address-book-button = Nastavi imenik
account-hub-chat-button = Nastavi klepet
account-hub-feed-button = Nastavi vir RSS
account-hub-newsgroup-button = Nastavi novičarsko skupino
account-hub-import-button = Uvozi profil
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Prijava v sinhronizacijo …

## Email page

account-hub-email-title = Nastavite račun za e-pošto
account-hub-email-cancel-button = Prekliči
account-hub-email-back-button = Nazaj
account-hub-email-continue-button = Nadaljuj
account-hub-email-confirm-button = Potrdi
