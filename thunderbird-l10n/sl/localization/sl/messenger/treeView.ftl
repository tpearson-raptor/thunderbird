# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-delete =
    .title = Izbriši trenutno vrstico
tree-list-view-column-picker =
    .title = Izberite stolpce za prikaz
tree-list-view-row-thread =
    .alt = Kazalnik niti sporočil
    .title = To je nit sporočil
tree-list-view-row-attach =
    .alt = Kazalnik priponk
    .title = Sporočilo vsebuje eno ali več priponk
