# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Encoding warnings and errors
EncNoDeclarationFrame=لم يُحدد ترميز مستند مُؤطّر. قد يظهر المستند بشكل مختلف إذا عرض خارج المستند الذي يُؤطّره.
EncMetaUnsupported=حُدّد ترميز محارف غير مدعوم لمستند HTML باستخدام وسم meta. تجاهلتُ التحديد.
EncProtocolUnsupported=أُعلن ترميز محارف غير مدعوم في مستوى ميفاق النقل. تجاهلتُ الإعلان.
EncMetaUtf16=استُخدم وسم وصفي لإعلان ترميز المحارف على أنه UTF-16. فُسّر هذا على أنّه إعلان UTF-8 عوضا عن ذلك.
EncMetaUserDefined=استُخدم وسم وصفي لإعلان ترميز المحارف على أنه x-user-defined. فُسّر هذا على أنّه إعلان windows-1252 عوضا عن ذلك لغرض التوافقية مع الخطوط العتيقة غير المرمزة كما ينبغي عمدا. على هذا الموقع الانتقال إلى يونيكود.

# The bulk of the messages below are derived from
# https://hg.mozilla.org/projects/htmlparser/file/1f633cef7de7/src/nu/validator/htmlparser/impl/ErrorReportingTokenizer.java
# which is available under the MIT license.

# Tokenizer errors
errGarbageAfterLtSlash=مهملات بعد ”‎</‎“.
errLtSlashGt=رأيتُ ”</>“. الأسباب المحتملة هي ”‎<‎“ لم يُهرّب (هرّبه مستخدما ”‎&lt;‎“) أو وسم نهاية أخطأت بكتابته.
errCharRefLacksSemicolon=المرجع المحرفي لم يُنهى بفاصلة منقوطة.
errNoSpaceBetweenAttributes=لا مسافة بين الصفات.
errAttributeValueMissing=قيمة الخاصية ناقصة.
errBadCharBeforeAttributeNameLt=رأيتُ ”‎<‎“ بينما توقعت اسم خاصية. السبب المحتمل هو عدم وجود ”‎>‎“ قبله مباشرة.
errEqualsSignBeforeAttributeName=رأيتُ ”=“ بينما توقعت اسم خاصية. السبب المحتمل هو عدم وجود اسم الخاصية.
errBadCharAfterLt=محرف سيئ بعد ”‎<‎“. السبب المحتمل هو ”‎<‎“ لم يُهرّب. جرّب تهريبه مستخدما ”‎&lt;‎“.
errLtGt=رأيتُ ”<>“. الأسباب المحتملة هي ”‎<‎“ لم يُهرّب (هرّبه مستخدما ”‎&lt;‎“) أو وسم بداية أخطأت بكتابته.
errProcessingInstruction=رأيتُ ”‎<?‎“. السبب المحتمل هو محاولة استخدام تعليمة معالجة XML في HTML. (تعليمات معالجة XML غير مدعومة في HTML.)
errQuoteBeforeAttributeName=رأيتُ علامة اقتباس بينما توقعت اسم خاصية. السبب المحتمل هو عدم وجود ”=“ قبله مباشرة.
errLtInAttributeName=”‎<‎“ في اسم الخاصية. السبب المحتمل هو عدم وجود ”‎>‎“ قبله مباشرة.
errQuoteInAttributeName=علامة اقتباس في اسم الصفة. المشكلة قد تكون: نقص علامة اقتباس مطابقة في مكان ما سبق هذا.
errEofInComment=انتهى الملف داخل تعليق.
errEofInDoctype=انتهى الملف داخل doctype.
errEofInAttributeValue=وصلتُ إلى نهاية الملف داخل قيمة صفة. سأتجاهل الوسم.
errEofInAttributeName=انتهى الملف عند اسم صفة. سأتجاهل الوسم.
errEofInEndTag=انتهى الملف داخل وسم نهاية. سأتجاهل الوسم.
errEofAfterLt=انتهى الملف بعد ”‎<‎“.
errDuplicateAttribute=صفة مكرّرة.
errMissingSpaceBeforeDoctypeName=مسافة ناقصة قبل اسم doctype.

# Tree builder errors
errUnclosedElements=رأيتُ وسم نهاية ”%1$S“، ولكن لم تكن هناك عناصر مفتوحة.
errUnclosedElementsImplied=رأيتُ وسم نهاية ”%1$S“ مفترض، و لكن لم تكن هناك عناصر مفتوحة.
errEndTagAfterBody=رأيتُ وسم نهاية بعد إغلاق ”body“.
errNoTableRowToClose=لا صف جدول لإغلاقه.
errEndWithUnclosedElements=رأيتُ وسم نهاية ”%1$S“، ولكن لم تكن هناك عناصر مفتوحة.

EncXmlDecl=The character encoding of an HTML document was declared using the XML declaration syntax. This is non-conforming, and declaring the encoding using a meta tag at the start of the head part is more efficient.
EncMetaTooLate=A meta tag attempting to declare the character encoding declaration was found too late, and the encoding was guessed from content instead. The meta tag needs to be moved to the start of the head part of the document.
EncMetaTooLateFrame=A meta tag attempting to declare the character encoding declaration was found too late, and the encoding of the parent document was used instead. The meta tag needs to be moved to the start of the head part of the document.
EncMetaAfterHeadInKilobyte=The meta tag declaring the character encoding of the document should be moved to start of the head part of the document.
EncNoDecl=The character encoding of the document was not declared, so the encoding was guessed from content. The character encoding needs to be declared in the Content-Type HTTP header, using a meta tag, or using a byte order mark.
EncNoDeclPlain=The character encoding of the document was not declared, so the encoding was guessed from content. The character encoding needs to be declared in the Content-Type HTTP header or using a byte order mark.
EncMetaReplacement=A meta tag was used to declare an encoding that is a cross-site scripting hazard. The replacement encoding was used instead.
EncProtocolReplacement=An encoding that is a cross-site scripting hazard was declared on the transfer protocol level. The replacement encoding was used instead.
EncDetectorReload=The character encoding of the document was not declared, and the encoding was guessable from content only late. This caused the document to be reloaded. The character encoding needs to be declared in the Content-Type HTTP header, using a meta tag, or using a byte order mark.
EncDetectorReloadPlain=The character encoding of the document was not declared, and the encoding was guessable from content only late. This caused the document to be reloaded. The character encoding needs to be declared in the Content-Type HTTP header or using a byte order mark.
EncError=The byte stream was erroneous according to the character encoding that was declared. The character encoding declaration may be incorrect.
EncErrorFrame=The byte stream was erroneous according to the character encoding that was inherited from the parent document. The character encoding needs to be declared in the Content-Type HTTP header, using a meta tag, or using a byte order mark.
EncErrorFramePlain=The byte stream was erroneous according to the character encoding that was inherited from the parent document. The character encoding needs to be declared in the Content-Type HTTP header or using a byte order mark.
EncSpeculationFailMeta=The start of the document was reparsed, because there were non-ASCII characters before the meta tag that declared the encoding. The meta should be the first child of head without non-ASCII comments before.
EncSpeculationFailXml=The start of the document was reparsed, because there were non-ASCII characters in the part of the document that was unsuccessfully searched for a meta tag before falling back to the XML declaration syntax. A meta tag at the start of the head part should be used instead of the XML declaration syntax.
# The audience of the following message isn't the author of the document but other people debugging browser behavior.
EncSpeculationFail2022=The start of the document was reparsed, because ISO-2022-JP is an ASCII-incompatible encoding.
errNoDigitsInNCR=No digits in numeric character reference.
errGtInSystemId=“>” in system identifier.
errGtInPublicId=“>” in public identifier.
errNamelessDoctype=Nameless doctype.
errConsecutiveHyphens=Consecutive hyphens did not terminate a comment. “--” is not permitted inside a comment, but e.g. “- -” is.
errPrematureEndOfComment=Premature end of comment. Use “-->” to end a comment properly.
errBogusComment=Bogus comment.
errUnquotedAttributeLt=“<” in an unquoted attribute value. Probable cause: Missing “>” immediately before.
errUnquotedAttributeGrave=“`” in an unquoted attribute value. Probable cause: Using the wrong character as a quote.
errUnquotedAttributeQuote=Quote in an unquoted attribute value. Probable causes: Attributes running together or a URL query string in an unquoted attribute value.
errUnquotedAttributeEquals=“=” in an unquoted attribute value. Probable causes: Attributes running together or a URL query string in an unquoted attribute value.
errSlashNotFollowedByGt=A slash was not immediately followed by “>”.
errUnquotedAttributeStartLt=“<” at the start of an unquoted attribute value. Probable cause: Missing “>” immediately before.
errUnquotedAttributeStartGrave=“`” at the start of an unquoted attribute value. Probable cause: Using the wrong character as a quote.
errUnquotedAttributeStartEquals=“=” at the start of an unquoted attribute value. Probable cause: Stray duplicate equals sign.
errUnescapedAmpersandInterpretedAsCharacterReference=The string following “&” was interpreted as a character reference. (“&” probably should have been escaped as “&amp;”.)
errNotSemicolonTerminated=Named character reference was not terminated by a semicolon. (Or “&” should have been escaped as “&amp;”.)
errNoNamedCharacterMatch=“&” did not start a character reference. (“&” probably should have been escaped as “&amp;”.)
errExpectedPublicId=Expected a public identifier but the doctype ended.
errBogusDoctype=Bogus doctype.
maybeErrAttributesOnEndTag=End tag had attributes.
maybeErrSlashInEndTag=Stray “/” at the end of an end tag.
errNcrNonCharacter=Character reference expands to a non-character.
errNcrSurrogate=Character reference expands to a surrogate.
errNcrControlChar=Character reference expands to a control character.
errNcrCr=A numeric character reference expanded to carriage return.
errNcrInC1Range=A numeric character reference expanded to the C1 controls range.
errEofInPublicId=End of file inside public identifier.
errEofWithoutGt=Saw end of file without the previous tag ending with “>”. Ignoring tag.
errEofInTagName=End of file seen when looking for tag name. Ignoring tag.
errNcrOutOfRange=Character reference outside the permissible Unicode range.
errNcrUnassigned=Character reference expands to a permanently unassigned code point.
errEofInSystemId=End of file inside system identifier.
errExpectedSystemId=Expected a system identifier but the doctype ended.
errNestedComment=Saw “<!--” within a comment. Probable cause: Nested comment (not allowed).
errNcrZero=Character reference expands to zero.
errNoSpaceBetweenDoctypeSystemKeywordAndQuote=No space between the doctype “SYSTEM” keyword and the quote.
errNoSpaceBetweenPublicAndSystemIds=No space between the doctype public and system identifiers.
errNoSpaceBetweenDoctypePublicKeywordAndQuote=No space between the doctype “PUBLIC” keyword and the quote.
# Tree builder errors
errDeepTree=The document tree is too deep. The tree will be flattened to be 513 elements deep.
errStrayStartTag2=Stray start tag “%1$S”.
errStrayEndTag=Stray end tag “%1$S”.
errUnclosedElementsCell=A table cell was implicitly closed, but there were open elements.
errStrayDoctype=Stray doctype.
errAlmostStandardsDoctype=Almost standards mode doctype. Expected “<!DOCTYPE html>”.
errQuirkyDoctype=Quirky doctype. Expected “<!DOCTYPE html>”.
errAlmostStandardsDoctypeVerbose=This page is in Almost Standards Mode. Page layout may be impacted. For Standards Mode use “<!DOCTYPE html>”.
errQuirkyDoctypeVerbose=This page is in Quirks Mode. Page layout may be impacted. For Standards Mode use “<!DOCTYPE html>”.
errNonSpaceInTrailer=Non-space character in page trailer.
errNonSpaceAfterFrameset=Non-space after “frameset”.
errNonSpaceInFrameset=Non-space in “frameset”.
errNonSpaceAfterBody=Non-space character after body.
errNonSpaceInColgroupInFragment=Non-space in “colgroup” when parsing fragment.
errNonSpaceInNoscriptInHead=Non-space character inside “noscript” inside “head”.
errFooBetweenHeadAndBody=“%1$S” element between “head” and “body”.
errStartTagWithoutDoctype=Start tag seen without seeing a doctype first. Expected “<!DOCTYPE html>”.
errNoSelectInTableScope=No “select” in table scope.
errStartSelectWhereEndSelectExpected=“select” start tag where end tag expected.
errStartTagWithSelectOpen=“%1$S” start tag with “select” open.
errBadStartTagInNoscriptInHead=Bad start tag “%1$S” in “noscript” in “head”.
errImage=Saw a start tag “image”.
errFooSeenWhenFooOpen2=Start tag “%1$S” seen but an element of the same type was already open.
errHeadingWhenHeadingOpen=Heading cannot be a child of another heading.
errFramesetStart=“frameset” start tag seen.
errNoCellToClose=No cell to close.
errStartTagInTable=Start tag “%1$S” seen in “table”.
errFormWhenFormOpen=Saw a “form” start tag, but there was already an active “form” element. Nested forms are not allowed. Ignoring the tag.
errTableSeenWhileTableOpen=Start tag for “table” seen but the previous “table” is still open.
errStartTagInTableBody=“%1$S” start tag in table body.
errEndTagSeenWithoutDoctype=End tag seen without seeing a doctype first. Expected “<!DOCTYPE html>”.
errEndTagSeenWithSelectOpen=“%1$S” end tag with “select” open.
errGarbageInColgroup=Garbage in “colgroup” fragment.
errEndTagBr=End tag “br”.
errNoElementToCloseButEndTagSeen=No “%1$S” element in scope but a “%1$S” end tag seen.
errHtmlStartTagInForeignContext=HTML start tag “%1$S” in a foreign namespace context.
errNonSpaceInTable=Misplaced non-space characters inside a table.
errUnclosedChildrenInRuby=Unclosed children in “ruby”.
errStartTagSeenWithoutRuby=Start tag “%1$S” seen without a “ruby” element being open.
errSelfClosing=Self-closing syntax (“/>”) used on a non-void HTML element. Ignoring the slash and treating as a start tag.
errNoCheckUnclosedElementsOnStack=Unclosed elements on stack.
errEndTagDidNotMatchCurrentOpenElement=End tag “%1$S” did not match the name of the current open element (“%2$S”).
errEndTagViolatesNestingRules=End tag “%1$S” violates nesting rules.
errListUnclosedStartTags=Unclosed element or elements.
