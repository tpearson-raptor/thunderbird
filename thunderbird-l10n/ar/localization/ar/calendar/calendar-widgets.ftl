# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = كلّ التقويمات معطّلة حاليًا. فعّل أحد التقويمات الموجودة أو أضِف واحدًا لإنشاء الأحداث وتحريرها.
calendar-deactivated-notification-tasks = كلّ التقويمات معطّلة حاليًا. فعّل أحد التقويمات الموجودة أو أضِف واحدًا لإنشاء المهام وتحريرها.
calendar-view-toggle-day = اليوم
    .title = انتقل إلى منظور اليوم
calendar-view-toggle-week = الأسبوع
    .title = انتقل إلى منظور الأسبوع
calendar-view-toggle-multiweek = الأسابيع المتعددة
    .title = انتقل إلى منظور الأسابيع المتعددة
calendar-view-toggle-month = الشهر
    .title = انتقل إلى منظور الشهر
