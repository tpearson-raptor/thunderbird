# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = 当前已禁用所有日历。启用现有日历或添加新日历以创建和编辑事件。
calendar-deactivated-notification-tasks = 当前已禁用所有日历。启用现有日历或添加新日历以创建和编辑事件。
calendar-notifications-label = 显示即将到来的事件通知
calendar-add-notification-button =
    .label = 新建通知
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = { $calendarName } 日历选项
calendar-import-new-calendar = 新建日历
    .title = 创建或订阅新日历
calendar-refresh-calendars =
    .title = 重新加载所有日历并同步更改
calendar-new-event-primary-button = 新建事件
calendar-new-task-primary-button = 新建任务
calendar-today-button-tooltip =
    .title = 转至今日
calendar-view-toggle-day = 日
    .title = 切换到日视图
calendar-view-toggle-week = 周
    .title = 切换到周视图
calendar-view-toggle-multiweek = 多周
    .title = 切换到多周视图
calendar-view-toggle-month = 月
    .title = 切换到月视图
