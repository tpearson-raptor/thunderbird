# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar strings


## Search bar


## Unified toolbar context menu

customize-menu-customize =
    .label = 自定义…

## Unified Toolbar customization

customize-space-tab-mail = 邮件
    .title = 邮件
customize-space-tab-addressbook = 通讯录
    .title = 通讯录
customize-space-tab-calendar = 日历
    .title = 日历
customize-space-tab-tasks = 任务
    .title = 任务
customize-space-tab-chat = 聊天
    .title = 聊天
customize-space-tab-settings = 设置
    .title = 设置
customize-button-style-icons-beside-text =
    .label = 文字旁的图示

## Unified toolbar customization palette context menu


## Unified toolbar customization target context menu

