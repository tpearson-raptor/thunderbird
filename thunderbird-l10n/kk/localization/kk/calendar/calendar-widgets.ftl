# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Барлық күнтізбелер қазір сөндірілген. Оқиғаларды қосу немесе түзету үшін бар болып тұрған күнтізбені іске қосыңыз, немесе жаңасын қосыңыз.
calendar-deactivated-notification-tasks = Барлық күнтізбелер қазір сөндірілген. Тапсырмаларды қосу немесе түзету үшін бар болып тұрған күнтізбені іске қосыңыз, немесе жаңасын қосыңыз.
calendar-notifications-label = Алдағы оқиғалар туралы ескертулерді көрсету
calendar-add-notification-button =
    .label = Ескертуді қосу

## Side panel

calendar-list-header = Күнтізбелер
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = { $calendarName } күнтізбе опциясы
calendar-import-new-calendar = Жаңа күнтізбе…
    .title = Жаңа күнтізбені жасау немесе жазылу
calendar-refresh-calendars =
    .title = Барлық күнтізбелерді қайта жүктеу және өзгерістерді синхрондау
calendar-new-event-primary-button = Жаңа оқиға
calendar-new-task-primary-button = Жаңа тапсырма

## Calendar navigation

calendar-nav-button-prev-tooltip-day =
    .title = Алдыңғы күн
    .accesskey = л
calendar-nav-button-prev-tooltip-week =
    .title = Алдыңғы апта
    .accesskey = л
calendar-nav-button-prev-tooltip-multiweek =
    .title = Алдыңғы апта
    .accesskey = л
calendar-nav-button-prev-tooltip-month =
    .title = Алдыңғы ай
    .accesskey = л
calendar-nav-button-next-tooltip-day =
    .title = Келесі күн
    .accesskey = к
calendar-nav-button-next-tooltip-week =
    .title = Келесі апта
    .accesskey = к
calendar-nav-button-next-tooltip-multiweek =
    .title = Келесі апта
    .accesskey = к
calendar-nav-button-next-tooltip-month =
    .title = Келесі ай
    .accesskey = к
calendar-today-button-tooltip =
    .title = Бүгінге өту
calendar-view-toggle-day = Күн
    .title = Күн көрінісіне ауысу
calendar-view-toggle-week = Апта
    .title = Апта көрінісіне ауысу
calendar-view-toggle-multiweek = Бірнеше апта
    .title = Бірнеше апта көрінісіне ауысу
calendar-view-toggle-month = Ай
    .title = Ай көрінісіне ауысу
