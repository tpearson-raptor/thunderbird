# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = <span data-l10n-name="brand-name">{ -brand-full-name }</span> ішіне қош келдіңіз
account-hub-title = Тіркелгілер орталығы

## Footer

account-hub-release-notes = Шығарылым ескертпесі
account-hub-support = Қолдау
account-hub-donate = Демеушілік ету

## Start page

account-hub-email-button = Эл. пошта тіркелгісін баптау
account-hub-new-email-button = Жаңа эл. пошта адресін алу
account-hub-calendar-button = Күнтізбені баптау
account-hub-address-book-button = Адрестік кітапшаны баптау
account-hub-chat-button = Чатты баптау
account-hub-feed-button = RSS таспасын баптау
account-hub-newsgroup-button = Жаңалықтар тобын баптау
account-hub-import-button = Профильді импорттау
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Синхрондау ішіне кіру…

## Email page

account-hub-email-title = Эл. пошта тіркелгіңізді баптау
account-hub-email-cancel-button = Бас тарту
account-hub-email-back-button = Артқа
account-hub-email-continue-button = Жалғастыру
account-hub-email-confirm-button = Растау
