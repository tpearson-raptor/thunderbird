# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header lists

message-header-to-list-name = Кімге
message-header-from-list-name = Кімнен
message-header-sender-list-name = Жiберушi
message-header-reply-to-list-name = Кімге жауап беру
message-header-cc-list-name = Көшірме
message-header-bcc-list-name = Жасырын көшірме
message-header-newsgroups-list-name = Жаңалықтар топтары
message-header-tags-list-name = Тегтер

## Other message headers.
## The field-separator is for screen readers to separate the field name from the field value.

message-header-author-field = Авторы<span data-l10n-name="field-separator">:</span>
message-header-organization-field = Ұйым<span data-l10n-name="field-separator">:</span>
message-header-subject-field = Тақырыбы<span data-l10n-name="field-separator">:</span>
message-header-followup-to-field = Келесіге қосымша<span data-l10n-name="field-separator">:</span>
message-header-date-field = Күні<span data-l10n-name="field-separator">:</span>
message-header-user-agent-field = Пайдаланушы агенті<span data-l10n-name="field-separator">:</span>
message-header-references-field = Сілтемелер<span data-l10n-name="field-separator">:</span>
message-header-message-id-field = Хабарлама анықтағышы<span data-l10n-name="field-separator">:</span>
message-header-in-reply-to-field = Келесіге жауап<span data-l10n-name="field-separator">:</span>
message-header-website-field = Веб-сайт<span data-l10n-name="field-separator">:</span>
# An additional email header field that the user has chosen to display. Unlike
# the other headers, the name of this header is not expected to be localised
# because it is generated from the raw field name found in the email header.
#   $fieldName (String) - The field name.
message-header-custom-field = { $fieldName }<span data-l10n-name="field-separator">:</span>

##

message-header-address-in-address-book-icon2 =
    .alt = Адрестік кітапшада бар
message-header-address-not-in-address-book-icon2 =
    .alt = Адрестік кітапшада жоқ
message-header-address-not-in-address-book-button =
    .title = Бұл адресті адрестік кітапшада сақтау
message-header-address-in-address-book-button =
    .title = Контактты түзету
message-header-field-show-more = Көбірек
    .title = Барлық алушыларды көрсету
message-ids-field-show-all = Барлығын көрсету
