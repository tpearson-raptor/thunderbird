# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

openpgp-change-key-expiry-title =
    .title = Кілттің жарамдылық мерзімін өзгерту
info-will-expire = Бұл кілт { $date } күні аяқталатындай бапталған.
info-already-expired = Бұл кілттің мерзімі өтіп кеткен.
info-does-not-expire = Бұл кілт мерзімі ешқашан аяқталмайтындай бапталған.
info-explanation-1 = <b>Кілттің мерзімі өткеннен кейін</b> оны шифрлеу немесе цифрлық қолтаңба үшін пайдалану мүмкін болмайды.
info-explanation-2 = Бұл кілтті ұзақтау уақыт бойы пайдалану үшін оның жарамдылық мерзімін өзгертіп, оның ашық кілтін сөйлесу серіктестеріңізге қайта жіберіңіз.
expire-dont-change =
    .label = Жарамдылық мерзімін өзгертпеу
expire-never-label =
    .label = Кілттің жарамдылық мерзімі ешқашан бітпейді
expire-in-label =
    .label = Кілттің жарамдылық мерзімі бітеді:
expire-in-months = Ай
