# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar Item Label strings

toolbar-synchronize-label = Synchronize
toolbar-delete-event-label = Delete
toolbar-go-to-today-label = Go to Today
toolbar-print-event-label = Print
toolbar-new-event-label = Event
toolbar-new-task-label = Task
