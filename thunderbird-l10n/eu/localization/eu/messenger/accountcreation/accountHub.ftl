# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Ongi etorri <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Kontuen gune nagusia

## Footer

account-hub-release-notes = Bertsio-oharrak
account-hub-support = Laguntza
account-hub-donate = Dohaintza egin

## Start page

account-hub-email-button = Konfiguratu posta elektroniko kontua
account-hub-new-email-button = Eskuratu posta elektroniko berri bat
account-hub-calendar-button = Konfiguratu egutegia
account-hub-address-book-button = Konfiguratu helbide-liburua
account-hub-chat-button = Konfiguratu txata
account-hub-feed-button = Konfiguratu RSS jarioak
account-hub-newsgroup-button = Konfiguratu berri-taldeak
account-hub-import-button = Inportatu profila
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Hasi saioa sinkronizatzeko…

## Email page

account-hub-email-title = Konfiguratu zure posta elektroniko kontua
account-hub-email-cancel-button = Utzi
account-hub-email-back-button = Atzera
account-hub-email-continue-button = Jarraitu
account-hub-email-confirm-button = Berretsi
