# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Kontrol-laukia txandakatzeko uneko errenkada aukeratua
    .title = Hautatu uneko errenkada
tree-list-view-row-deselect =
    .alt = Kontrol-laukia txandakatzeko uneko errenkada aukeratua
    .title = Desautatu uneko errenkada
tree-list-view-row-delete =
    .title = Ezabatu uneko errenkada
tree-list-view-column-picker =
    .title = Hautatu bistaratzeko zutabeak
tree-list-view-column-picker-restore =
    .label = Berrezarri zutabeen ordena
tree-list-view-row-thread =
    .alt = Mezu hariaren adierazlea
    .title = Hau mezu hariduna da
tree-list-view-row-flagged =
    .alt = Mezu izardunaren adierazlea
    .title = Izardun mezua
tree-list-view-row-flag =
    .alt = Mezu izardunaren adierazlea
    .title = Mezu izargabea
tree-list-view-row-attach =
    .alt = Eranskinaren adierazlea
    .title = Mezuak eranskinak ditu
tree-list-view-row-spam =
    .alt = Spam egoeraren adierazlea
    .title = Mezua Spam bezala markatu da
tree-list-view-row-not-spam =
    .alt = Spam egoeraren adierazlea
    .title = Mezua ez da Spam markatu
