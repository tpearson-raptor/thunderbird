# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Orain egutegi guztiak desgaituak daude. Gaitu badagoen egutegi bat edo gehitu egutegi berri bat gertaerak sortu eta editatzeko.
calendar-deactivated-notification-tasks = Orain egutegi guztiak desgaituak daude. Gaitu badagoen egutegi bat edo gehitu egutegi berri bat zereginak sortu eta editatzeko.
calendar-notifications-label = Erakutsi jakinarazpenak hurrengo gertaerentzat
calendar-add-notification-button =
    .label = Gehitu jakinarazpena
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = { $calendarName } egutegi aukera
calendar-import-new-calendar = Egutegi berria…
    .title = Sortu edo harpidetu egutegi berri batera
calendar-refresh-calendars =
    .title = Berritu egutegi guztiak eta sinkronizatu aldaketak
calendar-new-event-primary-button = Gertaera berria
calendar-new-task-primary-button = Zeregin berria
calendar-nav-button-prev-tooltip-day =
    .title = Aurreko eguna
    .accesskey = o
calendar-nav-button-prev-tooltip-week =
    .title = Aurreko astea
    .accesskey = o
calendar-nav-button-prev-tooltip-multiweek =
    .title = Aurreko astea
    .accesskey = o
calendar-nav-button-prev-tooltip-month =
    .title = Aurreko hilabetea
    .accesskey = o
calendar-nav-button-next-tooltip-day =
    .title = Hurrengo eguna
    .accesskey = h
calendar-nav-button-next-tooltip-week =
    .title = Hurrengo astea
    .accesskey = h
calendar-nav-button-next-tooltip-multiweek =
    .title = Hurrengo astea
    .accesskey = h
calendar-nav-button-next-tooltip-month =
    .title = Hurrengo hilabetea
    .accesskey = h
calendar-today-button-tooltip =
    .title = Gaur kokatu
calendar-view-toggle-day = Egun
    .title = Aldatu eguneko ikuspegira
calendar-view-toggle-week = Aste
    .title = Aldatu asteko ikuspegira
calendar-view-toggle-multiweek = Aste anitz
    .title = Aldatu aste anitzeko ikuspegira
calendar-view-toggle-month = Hilabete
    .title = Aldatu hileko ikuspegira
