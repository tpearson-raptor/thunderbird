# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Tutti i calendari sono attualmente disattivati. Attivare un calendario esistente o aggiungerne uno nuovo per creare e modificare eventi.
calendar-deactivated-notification-tasks = Tutti i calendari sono attualmente disattivati. Attivare un calendario esistente o aggiungerne uno nuovo per creare e modificare attività.
calendar-notifications-label = Mostra le notifiche per gli eventi futuri
calendar-add-notification-button =
    .label = Aggiungi notifica

## Side panel

calendar-list-header = Calendari
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = Opzione per calendario { $calendarName }
calendar-import-new-calendar = Nuovo calendario…
    .title = Crea o iscriviti a un nuovo calendario
calendar-refresh-calendars =
    .title = Ricarica tutti i calendari e sincronizza le modifiche
calendar-new-event-primary-button = Nuovo evento
calendar-new-task-primary-button = Nuova attività

## Calendar navigation

calendar-nav-button-prev-tooltip-day =
    .title = Giorno precedente
    .accesskey = r
calendar-nav-button-prev-tooltip-week =
    .title = Settimana precedente
    .accesskey = r
calendar-nav-button-prev-tooltip-multiweek =
    .title = Settimana precedente
    .accesskey = r
calendar-nav-button-prev-tooltip-month =
    .title = Mese precedente
    .accesskey = r
calendar-nav-button-next-tooltip-day =
    .title = Giorno successivo
    .accesskey = u
calendar-nav-button-next-tooltip-week =
    .title = Settimana successiva
    .accesskey = u
calendar-nav-button-next-tooltip-multiweek =
    .title = Settimana successiva
    .accesskey = u
calendar-nav-button-next-tooltip-month =
    .title = Mese successivo
    .accesskey = u
calendar-today-button-tooltip =
    .title = Vai a Oggi
calendar-view-toggle-day = Giorno
    .title = Passa alla vista giorno
calendar-view-toggle-week = Settimana
    .title = Passa alla vista settimana
calendar-view-toggle-multiweek = Multi-settimana
    .title = Passa alla vista multi-settimana
calendar-view-toggle-month = Mese
    .title = Passa alla vista mese
