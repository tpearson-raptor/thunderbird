# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Casella di controllo per selezionare la riga corrente
    .title = Seleziona la riga corrente
tree-list-view-row-deselect =
    .alt = Casella di controllo per deselezionare la riga corrente
    .title = Deseleziona la riga corrente
tree-list-view-row-delete =
    .title = Elimina la riga corrente
tree-list-view-column-picker =
    .title = Selezionare le colonne da mostrare
tree-list-view-column-picker-restore =
    .label = Ripristina l’ordine delle colonne
tree-list-view-row-thread =
    .alt = Indicatore messaggio in discussione
    .title = Questo messaggio è in una discussione
tree-list-view-row-flagged =
    .alt = Indicatore messaggio speciale
    .title = Messaggio contrassegnato come speciale
tree-list-view-row-flag =
    .alt = Indicatore messaggio speciale
    .title = Messaggio non contrassegnato come speciale
tree-list-view-row-attach =
    .alt = Indicatore allegato
    .title = Il messaggio contiene allegati
tree-list-view-row-spam =
    .alt = Indicatore stato posta indesiderata
    .title = Messaggio contrassegnato come posta indesiderata
tree-list-view-row-not-spam =
    .alt = Indicatore stato posta indesiderata
    .title = Messaggio non contrassegnato come posta indesiderata
