# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Benvenuto in <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Centro degli account

## Footer

account-hub-release-notes = Note di versione
account-hub-support = Supporto
account-hub-donate = Fai una donazione

## Start page

account-hub-email-button = Configura un account di posta
account-hub-new-email-button = Ottieni un nuovo indirizzo email
account-hub-calendar-button = Configura calendario
account-hub-address-book-button = Configura rubrica
account-hub-chat-button = Configura chat
account-hub-feed-button = Configura feed RSS
account-hub-newsgroup-button = Configura gruppo di discussione
account-hub-import-button = Importa profilo
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Accedi per sincronizzare…

## Email page

account-hub-email-title = Configura il tuo account di posta
account-hub-email-cancel-button = Annulla
account-hub-email-back-button = Indietro
account-hub-email-continue-button = Continua
account-hub-email-confirm-button = Conferma
