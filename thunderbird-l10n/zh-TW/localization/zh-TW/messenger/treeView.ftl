# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = 切換選擇目前這一列的選取盒
    .title = 選擇目前這一列
tree-list-view-row-deselect =
    .alt = 切換選擇目前這一列的選取盒
    .title = 取消選擇目前這一列
tree-list-view-row-delete =
    .title = 刪除目前這一列
tree-list-view-column-picker =
    .title = 選擇要顯示的欄位
tree-list-view-column-picker-restore =
    .label = 還原欄位順序
tree-list-view-row-thread =
    .alt = 討論串訊息圖示
    .title = 是討論串當中的訊息
tree-list-view-row-flagged =
    .alt = 星號標記圖示
    .title = 加上星號標記的訊息
tree-list-view-row-flag =
    .alt = 星號標記圖示
    .title = 未加上星號標記的訊息
tree-list-view-row-attach =
    .alt = 附件圖示
    .title = 含有附件的訊息
tree-list-view-row-spam =
    .alt = 垃圾信狀態圖示
    .title = 標示為垃圾信的訊息
tree-list-view-row-not-spam =
    .alt = 垃圾信狀態圖示
    .title = 標示為非垃圾信的訊息
