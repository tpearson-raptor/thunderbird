# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

## Table

tree-list-view-row-select =
    .alt = 現在の行の選択を切り替えるチェックボックス
    .title = 現在の行を選択します
tree-list-view-row-deselect =
    .alt = 現在の行の選択を切り替えるチェックボックス
    .title = 現在の行の選択を解除します
tree-list-view-row-delete =
    .title = 現在の行を削除します
tree-list-view-column-picker =
    .title = 表示する列を選択します
tree-list-view-column-picker-restore =
    .label = 列を元の順序に戻す
tree-list-view-row-thread =
    .alt = スレッドメッセージインジケーター
    .title = これはスレッド化されたメッセージです
tree-list-view-row-flagged =
    .alt = スター付きメッセージインジケーター
    .title = スター付きのメッセージです
tree-list-view-row-flag =
    .alt = スター付きメッセージインジケーター
    .title = スターが付いていないメッセージです
tree-list-view-row-attach =
    .alt = 添付ファイルインジケーター
    .title = メッセージに添付ファイルが含まれています
tree-list-view-row-spam =
    .alt = 迷惑メールインジケーター
    .title = 迷惑マーク付きのメッセージです
tree-list-view-row-not-spam =
    .alt = 迷惑メールインジケーター
    .title = 迷惑マークが付いていないメッセージです
