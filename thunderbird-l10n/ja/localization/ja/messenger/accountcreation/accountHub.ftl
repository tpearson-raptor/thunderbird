# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = <span data-l10n-name="brand-name">{ -brand-full-name }</span> へようこそ
account-hub-title = アカウントハブ

## Footer

account-hub-release-notes = リリースノート
account-hub-support = サポート
account-hub-donate = 寄付

## Start page

account-hub-email-button = メールアカウントをセットアップ
account-hub-new-email-button = 新しいメールアドレスを取得
account-hub-calendar-button = カレンダーをセットアップ
account-hub-address-book-button = アドレス帳をセットアップ
account-hub-chat-button = チャットをセットアップ
account-hub-feed-button = RSS フィードをセットアップ
account-hub-newsgroup-button = ニュースグループをセットアップ
account-hub-import-button = プロファイルをインポート
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Sync にログイン...

## Email page
account-hub-email-title = メールアカウントのセットアップ
account-hub-email-cancel-button = キャンセル
account-hub-email-back-button = 戻る
account-hub-email-continue-button = 続行
account-hub-email-confirm-button = 確認
