# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = 現在すべてのカレンダーが無効化されています。既存のカレンダーを有効にするか、新しいカレンダーを追加して予定を編集してください。
calendar-deactivated-notification-tasks = 現在すべてのカレンダーが無効化されています。既存のカレンダーを有効にするか、新しいカレンダーを追加して ToDo を編集してください。
calendar-notifications-label = 今後の予定の通知を表示する
calendar-add-notification-button =
    .label = 通知を追加
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = { $calendarName } カレンダーのオプション
calendar-import-new-calendar = 新しいカレンダー...
    .title = 新しいカレンダーを作成または登録します
calendar-refresh-calendars =
    .title = すべてのカレンダーを再読み込みして変更を同期します
calendar-new-event-primary-button = 新しい予定
calendar-new-task-primary-button = 新しい ToDo
calendar-nav-button-prev-tooltip-day =
    .title = 前の日
    .accesskey = s
calendar-nav-button-prev-tooltip-week =
    .title = 前の週
    .accesskey = s
calendar-nav-button-prev-tooltip-multiweek =
    .title = 前の週
    .accesskey = s
calendar-nav-button-prev-tooltip-month =
    .title = 前の月
    .accesskey = s
calendar-nav-button-next-tooltip-day =
    .title = 次の月
    .accesskey = x
calendar-nav-button-next-tooltip-week =
    .title = 次の週
    .accesskey = x
calendar-nav-button-next-tooltip-multiweek =
    .title = 次の週
    .accesskey = x
calendar-nav-button-next-tooltip-month =
    .title = 次の月
    .accesskey = x
calendar-today-button-tooltip =
    .title = 今日に移動します
calendar-view-toggle-day = 日
    .title = 日表示に切り替えます
calendar-view-toggle-week = 週
    .title = 週表示に切り替えます
calendar-view-toggle-multiweek = 多週
    .title = 多週表示に切り替えます
calendar-view-toggle-month = 月
    .title = 月表示に切り替えます
