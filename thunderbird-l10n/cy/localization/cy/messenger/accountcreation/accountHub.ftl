# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Croeso i <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Canolfan Cyfrifon

## Footer

account-hub-release-notes = Nodiadau rhyddhau
account-hub-support = Cefnogaeth
account-hub-donate = Cyfrannu

## Start page

account-hub-email-button = Creu cyfrif e-bost
account-hub-new-email-button = Cael cyfeiriad e-bost newydd
account-hub-calendar-button = Creu calendr
account-hub-address-book-button = Creu llyfr cyfeiriadau
account-hub-chat-button = Creu sgwrs
account-hub-feed-button = Creu ffrwd RSS
account-hub-newsgroup-button = Creu grŵp newyddion
account-hub-import-button = Mewnforio proffil
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Mewngofnodi i Sync…

## Email page

account-hub-email-title = Creu cyfrif e-bost
account-hub-email-cancel-button = Diddymu
account-hub-email-back-button = Nôl
account-hub-email-continue-button = Parhau
account-hub-email-confirm-button = Cadarnhau
