# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Y blwch ticio i newid dewis y rhes gyfredol
    .title = Dewis y rhes gyfredol
tree-list-view-row-deselect =
    .alt = Y blwch ticio i newid dewis y rhes gyfredol
    .title = Dad-ddewis y rhes gyfredol
tree-list-view-row-delete =
    .title = Dileu'r rhes gyfredol
tree-list-view-column-picker =
    .title = Dewis colofnau i'w dangos
tree-list-view-row-thread =
    .alt = Dangosydd edefyn newydd
    .title = Mae hon yn neges ar edefyn
tree-list-view-row-flagged =
    .alt = Dangosydd neges serennog
    .title = Neges serennog
tree-list-view-row-flag =
    .alt = Dangosydd neges serennog
    .title = Nid yw'n neges serennog
tree-list-view-row-attach =
    .alt = Dangosydd atodiadau
    .title = Mae'r neges yn cynnwys atodiad(au)
tree-list-view-row-spam =
    .alt = Dangosydd statws sbam
    .title = Neges wedi'i marcio fel sbam
tree-list-view-row-not-spam =
    .alt = Dangosydd statws sbam
    .title = Neges heb ei marcio fel sbam
