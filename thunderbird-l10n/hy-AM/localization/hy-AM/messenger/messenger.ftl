# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Window controls

about-rights-notification-text = { -brand-short-name }-ը անվճար և բաց կոդով ծրագրակազմ է, որը կառուցվել է աշխարհի տարբեր երկրների հազարավորների համայնքի կողմից:

## Content tabs

content-tab-security-high-icon =
    .alt = Կապակցումն անվտանգ է
content-tab-security-broken-icon =
    .alt = Կապակցումն անվտանգ չէ

## Toolbar

addons-and-themes-toolbarbutton =
    .label = Հավելումներ և ոճեր
    .tooltiptext = Կառավարել հավելումները

## Folder Pane


## Folder Toolbar Header Popup


## Menu


## AppMenu

appmenu-addons-and-themes =
    .label = Հավելումներ և ոճեր

## Context menu


## Message header pane


## Message header cutomize panel


## Action Button Context Menu


## Add-on removal warning


## no-reply handling


## error messages


## Spaces toolbar


## Spaces toolbar pinned tab menupopup


## Spaces toolbar customize panel


## Quick Filter Bar

# The label to display for the "View... Toolbars..." menu item that controls
# whether the quick filter bar is visible.
quick-filter-bar-toggle =
    .label = Արագ զտիչի վահանակ
    .accesskey = Ա
# This is the key used to show the quick filter bar.
# This should match quick-filter-bar-textbox-shortcut in about3Pane.ftl.
quick-filter-bar-show =
    .key = k
