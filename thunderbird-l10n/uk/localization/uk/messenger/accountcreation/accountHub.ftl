# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Вітаємо в <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Центр облікових записів

## Footer

account-hub-release-notes = Примітки до випуску
account-hub-support = Підтримка
account-hub-donate = Зробити внесок

## Start page

account-hub-email-button = Налаштувати обліковий запис електронної пошти
account-hub-new-email-button = Отримати нову електронну адресу
account-hub-calendar-button = Налаштувати календар
account-hub-address-book-button = Налаштувати адресну книгу
account-hub-chat-button = Налаштувати чат
account-hub-feed-button = Налаштувати RSS-канал
account-hub-newsgroup-button = Налаштувати групу новин
account-hub-import-button = Імпорт профілю
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Увійти до Синхронізації…

## Email page

account-hub-email-title = Налаштуйте свій обліковий запис електронної пошти
account-hub-email-cancel-button = Скасувати
account-hub-email-back-button = Назад
account-hub-email-continue-button = Продовжити
account-hub-email-confirm-button = Підтвердити
