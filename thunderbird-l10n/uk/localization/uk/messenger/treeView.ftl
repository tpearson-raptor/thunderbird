# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Прапорець для вибору поточного рядка
    .title = Вибрати поточний рядок
tree-list-view-row-deselect =
    .alt = Прапорець для вибору поточного рядка
    .title = Зняти вибір з поточного рядка
tree-list-view-row-delete =
    .title = Видалити поточний рядок
tree-list-view-column-picker =
    .title = Вибрати стовпчики для показу
tree-list-view-column-picker-restore =
    .label = Відновити послідовність стовпчиків
tree-list-view-row-thread =
    .alt = Покажчик повідомлення в розмові
    .title = Це повідомлення є частиною розмови
tree-list-view-row-flagged =
    .alt = Покажчик повідомлення із зірочкою
    .title = Повідомлення із зірочкою
tree-list-view-row-flag =
    .alt = Покажчик повідомлення із зірочкою
    .title = Повідомлення без зірочки
tree-list-view-row-attach =
    .alt = Покажчик вкладення
    .title = Повідомлення містить вкладення
tree-list-view-row-spam =
    .alt = Покажчик стану спаму
    .title = Повідомлення позначено як спам
tree-list-view-row-not-spam =
    .alt = Покажчик стану спаму
    .title = Повідомлення не позначено як спам
