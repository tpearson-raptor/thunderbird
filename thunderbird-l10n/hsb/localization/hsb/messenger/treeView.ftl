# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Kontrolny kašćik za přepinanje wuběra aktualneje linki
    .title = Aktualnu linku wubrać
tree-list-view-row-deselect =
    .alt = Kontrolny kašćik za přepinanje wuběra aktualneje linki
    .title = Aktualnu linku wotwolić
tree-list-view-row-delete =
    .title = Aktualnu linku zhašeć
tree-list-view-column-picker =
    .title = Wubjerće špalty, kotrež maja so zwobraznić
tree-list-view-column-picker-restore =
    .label = Porjad špaltow wobnowić
tree-list-view-row-thread =
    .alt = Indikator nitkoweje powěsće
    .title = To je powěsć w nitce
tree-list-view-row-flagged =
    .alt = Indikator za powěsće z hwěžku
    .title = Powěsć z hwěžku
tree-list-view-row-flag =
    .alt = Indikator za powěsće z hwěžku
    .title = Powěsć bjez hwěžki
tree-list-view-row-attach =
    .alt = Přiwěškowy indikator
    .title = Powěsć přiwěški wobsahuje
tree-list-view-row-spam =
    .alt = Indikator spamoweho statusa
    .title = Powěsć je so jako spam markěrowała
tree-list-view-row-not-spam =
    .alt = Indikator spamoweho statusa
    .title = Powěsć njeje so jako spam markěrowała
