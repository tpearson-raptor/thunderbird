# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Wšě protyki su tuchwilu znjemóžnjene. Zmóžńće eksistowacu protyku abo přidajće nowu, zo byšće podawki wutworił a wobdźěłał.
calendar-deactivated-notification-tasks = Wšě protyki su tuchwilu znjemóžnjene. Zmóžńće eksistowacu protyku abo přidajće nowu, zo byšće nadawki wutworił a wobdźěłał.
calendar-notifications-label = Zdźělenki za přichodne podawki pokazać
calendar-add-notification-button =
    .label = Zdźělenku přidać
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = Nastajenje protyki { $calendarName }
calendar-import-new-calendar = Nowa protyka…
    .title = Nowu protyku wutworić abo abonować
calendar-refresh-calendars =
    .title = Wšě protyki znowa začitać a změny synchronizować
calendar-new-event-primary-button = Nowy podawk
calendar-new-task-primary-button = Nowy nadawk
calendar-nav-button-prev-tooltip-day =
    .title = Předchadny dźeń
    .accesskey = e
calendar-nav-button-prev-tooltip-week =
    .title = Předchadny tydźeń
    .accesskey = t
calendar-nav-button-prev-tooltip-multiweek =
    .title = Předchadny tydźeń
    .accesskey = t
calendar-nav-button-prev-tooltip-month =
    .title = Předchadny měsac
    .accesskey = e
calendar-nav-button-next-tooltip-day =
    .title = Přichodny dźeń
    .accesskey = i
calendar-nav-button-next-tooltip-week =
    .title = Na nowy tydźeń
    .accesskey = n
calendar-nav-button-next-tooltip-multiweek =
    .title = Na nowy tydźeń
    .accesskey = n
calendar-nav-button-next-tooltip-month =
    .title = Přichodny měsac
    .accesskey = i
calendar-today-button-tooltip =
    .title = K dźensnišemu
calendar-view-toggle-day = Dźeń
    .title = Dnjowy napohlad wužiwać
calendar-view-toggle-week = Tydźeń
    .title = Tydźenski napohlad wužiwać
calendar-view-toggle-multiweek = Wjacetydźeń
    .title = Wjacetydźenski napohlad wužiwać
calendar-view-toggle-month = Měsac
    .title = Měsačny napohlad wužiwać
