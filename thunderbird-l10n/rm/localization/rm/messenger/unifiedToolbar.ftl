# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar strings


## Search bar


## Unified toolbar context menu

customize-menu-customize =
    .label = Modifitgar…

## Unified Toolbar customization

customize-space-tab-mail = E-mail
    .title = E-mail
customize-space-tab-addressbook = Cudeschet d'adressas
    .title = Cudeschet d'adressas
customize-space-tab-calendar = Chalender
    .title = Chalender
customize-space-tab-tasks = Incumbensas
    .title = Incumbensas
customize-space-tab-chat = Chat
    .title = Chat
customize-space-tab-settings = Parameters
    .title = Parameters
customize-button-style-icons-beside-text =
    .label = Simbols sper il text

## Unified toolbar customization palette context menu


## Unified toolbar customization target context menu

