# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar strings


## Search bar


## Unified toolbar context menu

customize-menu-customize =
    .label = Prilagodi…

## Unified Toolbar customization

customize-space-tab-mail = Pošta
    .title = Pošta
customize-space-tab-addressbook = Adresar
    .title = Adresar
customize-space-tab-calendar = Kalendar
    .title = Kalendar
customize-space-tab-tasks = Zadaci
    .title = Zadaci
customize-space-tab-chat = Razgovor
    .title = Razgovor
customize-space-tab-settings = Postavke
    .title = Postavke
customize-button-style-icons-beside-text =
    .label = Ikone uz tekst

## Unified toolbar customization palette context menu


## Unified toolbar customization target context menu

