# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Dobro došli u <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Početna stranica računa

## Footer

account-hub-release-notes = Napomene o izdanju
account-hub-support = Podrška
account-hub-donate = Doniraj

## Start page

account-hub-email-button = Postavite račun e-pošte
account-hub-new-email-button = Otvorite novu adresu e-pošte
account-hub-calendar-button = Postavite kalendar
account-hub-address-book-button = Postavite adresar
account-hub-chat-button = Postavite razgovor
account-hub-feed-button = Postavite RSS feed
account-hub-newsgroup-button = Postavite interesnu grupu
account-hub-import-button = Uvoz profila
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Prijavi se za sinkronizaciju…

## Email page

account-hub-email-title = Postavite svoj račun e-pošte
account-hub-email-cancel-button = Odustani
account-hub-email-back-button = Natrag
account-hub-email-continue-button = Nastavi
account-hub-email-confirm-button = Potvrdi
