# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Začiarkavacie políčko na prepnutie výberu aktuálneho riadka
    .title = Zvoliť aktuálny riadok
tree-list-view-row-deselect =
    .alt = Začiarkavacie políčko na prepnutie výberu aktuálneho riadka
    .title = Zrušiť označenie aktuálneho riadku
tree-list-view-row-delete =
    .title = Odstrániť aktuálny riadok
tree-list-view-column-picker =
    .title = Zvoliť stĺpce, ktoré chcete zobraziť
tree-list-view-column-picker-restore =
    .label = Obnoviť poradie stĺpcov
tree-list-view-row-thread =
    .alt = Indikátor správy vo vlákne
    .title = Toto je správa vo vlákne
tree-list-view-row-flagged =
    .alt = Indikátor správy označenej hviezdičkou
    .title = Správa označená hviezdičkou
tree-list-view-row-flag =
    .alt = Indikátor správy označenej hviezdičkou
    .title = Správa neoznačená hviezdičkou
tree-list-view-row-attach =
    .alt = Indikátor prílohy
    .title = Správa obsahuje prílohy
tree-list-view-row-spam =
    .alt = Indikátor príznaku nevyžiadanej pošty
    .title = Správa označená ako nevyžiadaná
tree-list-view-row-not-spam =
    .alt = Indikátor príznaku nevyžiadanej pošty
    .title = Správa neoznačená ako nevyžiadaná
