# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Víta vás <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Centrum účtov

## Footer

account-hub-release-notes = Poznámky k vydaniu
account-hub-support = Podpora
account-hub-donate = Prispieť

## Start page

account-hub-email-button = Nastaviť e-mailový účet
account-hub-new-email-button = Získať novú e-mailovú adresu
account-hub-calendar-button = Nastaviť kalendár
account-hub-address-book-button = Nastaviť adresár
account-hub-chat-button = Nastaviť konverzácie
account-hub-feed-button = Nastaviť kanál RSS
account-hub-newsgroup-button = Nastaviť diskusnú skupinu
account-hub-import-button = Importovať profil
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Prihlásiť sa a synchronizovať…

## Email page

account-hub-email-title = Nastavenie e-mailového účtu
account-hub-email-cancel-button = Zrušiť
account-hub-email-back-button = Naspäť
account-hub-email-continue-button = Pokračovať
account-hub-email-confirm-button = Potvrdiť
