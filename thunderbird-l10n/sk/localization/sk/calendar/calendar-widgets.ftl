# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Všetky kalendáre sú v súčasnosti zakázané. Pre pridanie alebo úpravu udalostí povoľte existujúci kalendár alebo pridajte nový.
calendar-deactivated-notification-tasks = Všetky kalendáre sú v súčasnosti zakázané. Pre pridanie alebo úpravu úloh povoľte existujúci kalendár alebo pridajte nový.
calendar-notifications-label = Zobrazovať upozornenia na nadchádzajúce udalosti
calendar-add-notification-button =
    .label = Pridať upozornenie
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = Predvoľba kalendára { $calendarName }
calendar-import-new-calendar = Nový kalendár…
    .title = Vytvorte alebo sa prihláste na odber nového kalendára
calendar-refresh-calendars =
    .title = Znova načítať obsah všetkých kalendárov a synchronizovať zmeny
calendar-new-event-primary-button = Nová udalosť
calendar-new-task-primary-button = Nová úloha
calendar-nav-button-prev-tooltip-day =
    .title = Predchádzajúci deň
    .accesskey = e
calendar-nav-button-prev-tooltip-week =
    .title = Predchádzajúci týždeň
    .accesskey = e
calendar-nav-button-prev-tooltip-multiweek =
    .title = Predchádzajúci týždeň
    .accesskey = e
calendar-nav-button-prev-tooltip-month =
    .title = Predchádzajúci mesiac
    .accesskey = e
calendar-nav-button-next-tooltip-day =
    .title = Nasledujúci deň
    .accesskey = a
calendar-nav-button-next-tooltip-week =
    .title = Nasledujúci týždeň
    .accesskey = a
calendar-nav-button-next-tooltip-multiweek =
    .title = Nasledujúci týždeň
    .accesskey = a
calendar-nav-button-next-tooltip-month =
    .title = Nasledujúci mesiac
    .accesskey = a
calendar-today-button-tooltip =
    .title = Prejsť na panel Dnes
calendar-view-toggle-day = Deň
    .title = Zmeniť pohľad na denný
calendar-view-toggle-week = Týždeň
    .title = Zmeniť pohľad na týždenný
calendar-view-toggle-multiweek = Viac týždňov
    .title = Zmeniť pohľad na viactýždenný
calendar-view-toggle-month = Mesiac
    .title = Zmeniť pohľad na mesačný
