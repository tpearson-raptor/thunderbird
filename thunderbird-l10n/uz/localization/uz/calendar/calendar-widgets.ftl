# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-view-toggle-day = Kun
    .title = Kunlik ko‘rinishga o‘tish
calendar-view-toggle-week = Hafta
    .title = Haftalik ko‘rinishga o‘tish
calendar-view-toggle-multiweek = Ko‘p haftalik
    .title = Ko‘p haftalik ko‘rinishga o‘tish
calendar-view-toggle-month = Oy
    .title = Oylik ko‘rinishga o‘tish
