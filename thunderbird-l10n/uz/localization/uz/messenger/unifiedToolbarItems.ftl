# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar Item Label strings

toolbar-synchronize-label = Sinxronlash
toolbar-synchronize =
    .title = Taqvimlarni qayta yuklash va o‘zgarishlarni sinxornlash
toolbar-delete-event-label = O‘chirish
toolbar-delete-event =
    .title = Tanlangan tadbir va vazifalarni o‘chirish
toolbar-go-to-today-label = Bugunga o‘tish
toolbar-go-to-today =
    .title = Bugunga o‘tish
toolbar-print-event-label = Chop qilish
toolbar-print-event =
    .title = Tadbir yoki vazifalarni o‘chirish
toolbar-new-event-label = Tadbir
toolbar-new-event =
    .title = Yangi tadbir yaratish
toolbar-new-task-label = Vazifa
toolbar-new-task =
    .title = Yangi vazifa yaratish
