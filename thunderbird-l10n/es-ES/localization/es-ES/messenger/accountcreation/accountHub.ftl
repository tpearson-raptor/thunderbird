# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Te damos la bienvenida a <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Centro de cuentas

## Footer

account-hub-release-notes = Notas de la versión
account-hub-support = Ayuda
account-hub-donate = Donar

## Start page

account-hub-email-button = Configurar una cuenta de correo electrónico
account-hub-new-email-button = Obtener una nueva dirección de correo electrónico
account-hub-calendar-button = Configurar el calendario
account-hub-address-book-button = Configurar la libreta de direcciones
account-hub-chat-button = Configurar el chat
account-hub-feed-button = Configurar el canal RSS
account-hub-newsgroup-button = Configurar el grupo de noticias
account-hub-import-button = Importar perfil
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Iniciar sesión en Sync…

## Email page

account-hub-email-title = Configure su cuenta de correo electrónico
account-hub-email-cancel-button = Cancelar
account-hub-email-back-button = Atrás
account-hub-email-continue-button = Continuar
account-hub-email-confirm-button = Confirmar
