# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Todas as agendas estão desativadas. Ative uma agenda existente ou adicione uma nova para criar e editar eventos.
calendar-deactivated-notification-tasks = Todas as agendas estão desativadas. Ative uma agenda existente ou adicione uma nova para criar e editar tarefas.
calendar-notifications-label = Exibir notificações de eventos próximos
calendar-add-notification-button =
    .label = Adicionar notificação

## Side panel

calendar-list-header = Agendas
# Variables:
#  $calendarName (String) - Calendar name as given by the user
calendar-list-item-tooltip =
    .title = Opção da agenda { $calendarName }
calendar-import-new-calendar = Nova agenda…
    .title = Criar ou se inscrever em uma nova agenda
calendar-refresh-calendars =
    .title = Recarregar todas as agendas e sincronizar alterações
calendar-new-event-primary-button = Novo evento
calendar-new-task-primary-button = Nova tarefa

## Calendar navigation

calendar-nav-button-prev-tooltip-day =
    .title = Dia anterior
    .accesskey = t
calendar-nav-button-prev-tooltip-week =
    .title = Semana anterior
    .accesskey = t
calendar-nav-button-prev-tooltip-multiweek =
    .title = Semana anterior
    .accesskey = t
calendar-nav-button-prev-tooltip-month =
    .title = Mês anterior
    .accesskey = t
calendar-nav-button-next-tooltip-day =
    .title = Próximo dia
    .accesskey = x
calendar-nav-button-next-tooltip-week =
    .title = Próxima semana
    .accesskey = x
calendar-nav-button-next-tooltip-multiweek =
    .title = Próxima semana
    .accesskey = x
calendar-nav-button-next-tooltip-month =
    .title = Próximo mês
    .accesskey = x
calendar-today-button-tooltip =
    .title = Ir para hoje
calendar-view-toggle-day = Dia
    .title = Mudar para visão do dia
calendar-view-toggle-week = Semana
    .title = Mudar para visão da semana
calendar-view-toggle-multiweek = Semanal
    .title = Mudar para visão semanal
calendar-view-toggle-month = Mês
    .title = Mudar para visão do mês
