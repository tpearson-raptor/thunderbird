# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Alternar seleção da linha atual
    .title = Selecionar a linha atual
tree-list-view-row-deselect =
    .alt = Alternar seleção da linha atual
    .title = Deixar de selecionar a linha atual
tree-list-view-row-delete =
    .title = Excluir a linha atual
tree-list-view-column-picker =
    .title = Selecionar colunas a exibir
tree-list-view-column-picker-restore =
    .label = Restaurar ordem das colunas
tree-list-view-row-thread =
    .alt = Indicador de mensagem agrupada
    .title = Esta é uma mensagem agrupada
tree-list-view-row-flagged =
    .alt = Indicador de mensagem com estrela
    .title = Mensagem marcada com estrela
tree-list-view-row-flag =
    .alt = Indicador de mensagem com estrela
    .title = Mensagem não marcada com estrela
tree-list-view-row-attach =
    .alt = Indicador de anexos
    .title = Mensagem contém anexos
tree-list-view-row-spam =
    .alt = Indicador de status de spam
    .title = Mensagem marcada como spam
tree-list-view-row-not-spam =
    .alt = Indicador de status de spam
    .title = Mensagem não marcada como spam
