# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Alle kalendrar er for tida deaktiverte. Aktiver ein eksisterande kalender, eller legg til ein ny for å opprette og redigere hendingar.
calendar-deactivated-notification-tasks = Alle kalendrar er akkurat no deaktiverte. Aktiver ein eksisterande kalender, eller legg til ein ny for å opprette og redigere oppgåver.
calendar-notifications-label = Vis varsel for komande hendingar
calendar-add-notification-button =
    .label = Legg til varsel

## Side panel

calendar-list-header = Kalendrar
calendar-new-event-primary-button = Ny hending
calendar-new-task-primary-button = Ny oppgåve

## Calendar navigation

calendar-view-toggle-day = Dag
    .title = Byt til dagvising
calendar-view-toggle-week = Veke
    .title = Byt til vekevising
calendar-view-toggle-multiweek = Fleire veker
    .title = Byt til fleirvekevising
calendar-view-toggle-month = Månad
    .title = Byt til månadvising
