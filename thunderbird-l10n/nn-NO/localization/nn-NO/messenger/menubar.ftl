# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-context-menu-menu-bar =
    .toolbarname = Menylinje
    .accesskey = M

## Tools Menu

menu-tools-settings =
    .label = Innstillingar
    .accesskey = I
menu-addons-and-themes =
    .label = Tillegg og tema
    .accesskey = T

## Help Menu

menu-help-share-feedback =
    .label = Del idear og tilbakemeldingar
    .accesskey = D
menu-help-enter-troubleshoot-mode =
    .label = Feilsøkingsmodus…
    .accesskey = F
menu-help-exit-troubleshoot-mode =
    .label = Slå av feilsøkingsmodus
    .accesskey = a
menu-help-more-troubleshooting-info =
    .label = Meir feilsøkingsinformasjon
    .accesskey = M
menu-help-troubleshooting-info =
    .label = Feilsøkingsinfo
    .accesskey = F

## Mail Toolbar

toolbar-junk-button =
    .label = Uønskt
    .tooltiptext = Merk dei valde meldingane som uønskt e-post
toolbar-not-junk-button =
    .label = Ønskt
    .tooltiptext = Merk dei valde meldingene som ønskt e-post
toolbar-delete-button =
    .label = Slett
    .tooltiptext = Slett valgte meldinger eller mappe
toolbar-undelete-button =
    .label = Angre sletting
    .tooltiptext = Angre sletting av valde meldingar

## View

menu-view-repair-text-encoding =
    .label = Reparer tekstkoding
    .accesskey = k

## View / Layout

menu-font-size-label =
    .label = Skriftstorleik
    .accesskey = S
menuitem-font-size-enlarge =
    .label = Auk skriftstorleik
    .accesskey = A
menuitem-font-size-reduce =
    .label = Reduser skriftstorleik
    .accesskey = R
menuitem-font-size-reset =
    .label = Still tilbake tekststorlleik
    .accesskey = t
mail-uidensity-label =
    .label = Densitet
    .accesskey = D
mail-uidensity-compact =
    .label = Kompakt
    .accesskey = K
mail-uidensity-normal =
    .label = Normal
    .accesskey = N
mail-uidensity-touch =
    .label = Tøtsj
    .accesskey = T
mail-uidensity-default =
    .label = Standard
    .accesskey = S
mail-uidensity-relaxed =
    .label = Avslappa
    .accesskey = A

## File

file-new-newsgroup-account =
    .label = Nyheitsgruppekonto
    .accesskey = k
