# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar strings


## Search bar


## Unified toolbar context menu

customize-menu-customize =
    .label = Pśiměriś…

## Unified Toolbar customization

customize-space-tab-mail = E-mail
    .title = E-mail
customize-space-tab-addressbook = Adresnik
    .title = Adresnik
customize-space-tab-calendar = Kalendaŕ
    .title = Kalendaŕ
customize-space-tab-tasks = Nadawki
    .title = Nadawki
customize-space-tab-chat = Chat
    .title = Chat
customize-space-tab-settings = Nastajenja
    .title = Nastajenja
customize-button-style-icons-beside-text =
    .label = Symbole a tekst

## Unified toolbar customization palette context menu


## Unified toolbar customization target context menu

