# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Witajśo k <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Kontowy centrum

## Footer

account-hub-release-notes = Wersijowe informacije
account-hub-support = Pomoc
account-hub-donate = Pósćiś

## Start page

account-hub-email-button = E-mailowe konto konfigurěrowaś
account-hub-new-email-button = Wobstarajśo se e-mailowu adresu
account-hub-calendar-button = Kalendaŕ konfigurěrowaś
account-hub-address-book-button = Adresnik konfigurěrowaś
account-hub-chat-button = Chat konfigurěrowaś
account-hub-feed-button = RSS-kanal konfigurěrowaś
account-hub-newsgroup-button = Diskusijnu kupku konfigurěrowaś
account-hub-import-button = Profil importěrowaś
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Pla Sync pśizjawiś…

## Email page

account-hub-email-title = Wašo e-mailowe konto konfigurěrowaś
account-hub-email-cancel-button = Pśetergnuś
account-hub-email-back-button = Slědk
account-hub-email-continue-button = Dalej
account-hub-email-confirm-button = Wobkšuśiś
