# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Welcome to <span data-l10n-name="brand-name">{ -brand-full-name }</span>
account-hub-title = Account Hub

## Footer

account-hub-release-notes = Release notes
account-hub-support = Support
account-hub-donate = Donate

## Start page

account-hub-email-button = Set up email account
account-hub-new-email-button = Get a new email address
account-hub-calendar-button = Set up calendar
account-hub-address-book-button = Set up address book
account-hub-chat-button = Set up chat
account-hub-feed-button = Set up RSS feed
account-hub-newsgroup-button = Set up newsgroup
account-hub-import-button = Import profile
# Note: "Sync" represents the Firefox Sync product so it shouldn't be translated.
account-hub-sync-button = Sign in to Sync…

## Email page

account-hub-email-title = Set up your email account
account-hub-email-cancel-button = Cancel
account-hub-email-back-button = Back
account-hub-email-continue-button = Continue
account-hub-email-confirm-button = Confirm
