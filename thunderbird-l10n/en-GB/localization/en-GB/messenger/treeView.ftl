# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Table

tree-list-view-row-select =
    .alt = Tickbox to toggle select the current row
    .title = Select the current row
tree-list-view-row-deselect =
    .alt = Tickbox to toggle select the current row
    .title = Deselect the current row
tree-list-view-row-delete =
    .title = Delete the current row
tree-list-view-column-picker =
    .title = Select columns to display
tree-list-view-column-picker-restore =
    .label = Restore column order
tree-list-view-row-thread =
    .alt = Thread message indicator
    .title = This is a threaded message
tree-list-view-row-flagged =
    .alt = Starred message indicator
    .title = Message starred
tree-list-view-row-flag =
    .alt = Starred message indicator
    .title = Message not starred
tree-list-view-row-attach =
    .alt = Attachment indicator
    .title = Message contains attachments
tree-list-view-row-spam =
    .alt = Spam status indicator
    .title = Message marked as spam
tree-list-view-row-not-spam =
    .alt = Spam status indicator
    .title = Message not marked as spam
