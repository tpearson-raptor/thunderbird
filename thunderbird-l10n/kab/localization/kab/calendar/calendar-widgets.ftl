# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-deactivated-notification-events = Imitayen meṛṛa nsan akka tura, Rmed awitay illan neɣ rnu amaynut akken ad ternuḍ neɣ ad tesnifleḍ tidyanin.
calendar-deactivated-notification-tasks = Imitayen meṛṛa nsan akka tura, Rmed awitay illan neɣ rnu amaynut akken ad ternuḍ neɣ ad tesnifleḍ tiwuriwin.
calendar-notifications-label = Sken ilɣa n tedyanin i d-iteddun
calendar-add-notification-button =
    .label = Rnu alɣu
calendar-view-toggle-day = Ass
    .title = Ddu ɣer tmeẓri n wass
calendar-view-toggle-week = Dduṛt
    .title = Ddu ɣer tmeẓri n dduṛt
calendar-view-toggle-multiweek = Deqs n dduṛtat
    .title = Ddu ɣer tmeẓri n deqs n dduṛtat
calendar-view-toggle-month = Aggur
    .title = Ddu ɣer tmeẓri n waggur
