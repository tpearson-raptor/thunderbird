# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-hub-brand = { -brand-full-name }
account-hub-welcome-line = Ansuf ɣer <span data-l10n-name="brand-name">{ -brand-full-name }</span>

## Footer

account-hub-release-notes = Iwenniten n lqem
account-hub-support = Tallalt
account-hub-donate = Mudd tawsa

## Start page

account-hub-email-button = Sbadu amiḍan n yimayl
account-hub-new-email-button = Awi tansa n yimayl tamaynut
account-hub-calendar-button = Sbadu awitay
account-hub-address-book-button = Sbadu  imedlis n tensa
account-hub-chat-button = Sbadu adiwenni

## Email page

