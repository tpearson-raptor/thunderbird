# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Unified Toolbar strings


## Search bar


## Unified toolbar context menu

customize-menu-customize =
    .label = Sagen…

## Unified Toolbar customization

customize-space-tab-mail = Imayl
    .title = Imayl
customize-space-tab-addressbook = Imedlis n tensa
    .title = Imedlis n tensa
customize-space-tab-calendar = Awitay
    .title = Awitay
customize-space-tab-tasks = Tiwuriwin
    .title = Tiwuriwin
customize-space-tab-chat = Adiwenni usrid
    .title = Adiwenni usrid
customize-space-tab-settings = Iɣewwaren
    .title = Iɣewwaren
customize-button-style-icons-beside-text =
    .label = Tigiiniyin n daw uḍris

## Unified toolbar customization palette context menu


## Unified toolbar customization target context menu

